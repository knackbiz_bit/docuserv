package com.docuserve;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import com.docuserve.dto.UserDTO;
import com.docuserve.response.UserResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import javafx.scene.paint.Stop;
import net.minidev.json.JSONUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.core.io.ClassPathResource;

import static org.apache.http.HttpHeaders.USER_AGENT;

/**
 * Hello world!
 *
 */
public class DocumentUploadTest {

	public static List<String> fileList(String directory) {
		List<String> fileNames = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
			for (Path path : directoryStream) {
				fileNames.add(path.toString());
			}
		} catch (IOException ex) {
			System.out.println(ex);
		}
		return fileNames;
	}

	public static void main(String[] args) throws Exception {
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		CloseableHttpClient httpClient = HttpClients.custom()
				.setConnectionManager(cm)
				.build();
		ExecutorService service = Executors.newFixedThreadPool(2);
		CompletionService<String> completionService = new ExecutorCompletionService<String>(service);
		File reportFile = new File("c:/users/venka/Desktop/report.xls");
		HSSFWorkbook book = new HSSFWorkbook();
		HSSFSheet sheet = book.createSheet();
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Image Id");
		header.createCell(1).setCellValue("Time Taken");
		class Index{
			int value = 0;
		}
		Index index = new Index();
		String token = loginUser();
		List<Future> futureList = Lists.newArrayList();
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		for(final String filePath : fileList("c:/users/venka/Desktop/higher")){

			if(index.value >= 25){
				break;
			}
			Row row = sheet.createRow(index.value);

			futureList.add(completionService.submit(new Callable<String>() {
				@Override
				public String call() throws Exception {
					File file = new File(filePath);
					InputStream stream = new FileInputStream(file);
					MultipartEntityBuilder builder = MultipartEntityBuilder.create();
					builder.addBinaryBody("file", stream,  ContentType.create("application/octet-stream"), "file");
					builder.addTextBody("documentParam", "sample text");
					HttpEntity entity = (HttpEntity) builder.build();
					HttpPost httpPost = new HttpPost("http://ec2-54-179-157-205.ap-southeast-1.compute.amazonaws.com:9090/docuserve/document/uploadDoc");
					httpPost.setEntity(entity);
					httpPost.setHeader("Authorization", "Bearer " + token);
					StopWatch watch = new StopWatch();
					watch.start();
					HttpResponse response = httpClient.execute(httpPost);
					HttpEntity result = response.getEntity();
					IOUtils.copy(result.getContent(), System.out);
					watch.stop();
					row.createCell(0).setCellValue(filePath.substring(filePath.lastIndexOf("\\")));
					row.createCell(1).setCellValue(watch.getTime());
					return null;
				}
			}));
			++index.value;
		}
		System.out.println("Future size :: " + futureList.size());
		for(Future<String> future : futureList){
			completionService.take().get();
		}
		stopWatch.stop();
		System.out.println("Total Time Taken for all images ************ :: " + stopWatch.getTime());
		service.shutdown();
		book.write(new FileOutputStream(reportFile));

	}

	private static String loginUser() throws Exception {
		String url ="http://ec2-54-179-157-205.ap-southeast-1.compute.amazonaws.com:9090/docuserve/users";
		HttpPost httpPost = new HttpPost(url);
		HttpEntity entity = new StringEntity("{\"emailId\": \"test@kelola.com\", \"password\": \"kelola\"}",ContentType.create("application/json"));
		httpPost.setEntity(entity);

		// add header
		httpPost.setHeader("Host", "ec2-54-179-157-205.ap-southeast-1.compute.amazonaws.com:9090");
		httpPost.setHeader("User-Agent", USER_AGENT);
		httpPost.setHeader("Accept",	"application/json, text/plain, */*");
		httpPost.setHeader("Accept-Language", "en-US,en;q=0.5");
		httpPost.setHeader("Connection", "keep-alive");
		httpPost.setHeader("Referer", "http://ec2-54-179-157-205.ap-southeast-1.compute.amazonaws.com:9090/docuserve/");
		httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");

		//post.setEntity(new UrlEncodedFormEntity());

		HttpClient httpClient = new DefaultHttpClient();

		HttpResponse response = httpClient.execute(httpPost);
		HttpEntity result = response.getEntity();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copy(result.getContent(), bos);
		String loginResponse = new String(bos.toByteArray());
		ObjectMapper mapper = new ObjectMapper();
		UserResponse dto = mapper.readValue(bos.toByteArray(), UserResponse.class);
		return dto.getToken();
	}
}
