package com.docuserve;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Hello world!
 *
 */
public class OCRTest {
	/*public static void main(String[] args) throws Exception {
		for(File file : new File("C:/Users/deepthi/Desktop/images/input_images/newset/type2").listFiles()){
			System.out.println("Processing file :: " + file.getName());
			if(!file.getName().contains("jpg"))
				continue;
			MultipartEntityBuilder builder = MultipartEntityBuilder.create()
					.addBinaryBody("file", file, ContentType.create("application/octet-stream"), "file");
			HttpEntity entity = (HttpEntity) builder.build();	
			StopWatch sw = new StopWatch();
			
			//HttpPost httpPost = new HttpPost("http://ec2-54-179-147-242.ap-southeast-1.compute.amazonaws.com:8888/ocr/process");
			HttpPost httpPost = new HttpPost("http://localhost:9090/docuserve/document/upload?documentParam=abc");
			//HttpPost httpPost = new HttpPost("http://43.245.187.130:9091/docuserve/document/upload?documentParam=abc");
			httpPost.setEntity(entity);
			httpPost.setHeader("Authorization","Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkdW1teUBlbWFpbC5jb20iLCJpYXQiOjE0OTY1ODczMDMsInN1YiI6ImRvY3VzZXJ2ZSIsImlzcyI6ImRvY3VzZXJ2ZSIsImV4cCI6MTQ5NjU5MDkwM30.-6fGBYMZpnjbsZNzNsqmPyObggcSoU2BckiDO1WjV4U");
			HttpClient httpClient = new DefaultHttpClient();
			sw.start();
			HttpResponse response = httpClient.execute(httpPost);
			sw.stop();
			HttpEntity result = response.getEntity();
			IOUtils.copy(result.getContent(), new FileOutputStream("C:/Users/deepthi/Desktop/images/output_images/" + file.getName() + ".tess"));
			System.out.println("Processed file :: " + file.getName());
		}
		
	}*/
	public static void main(String [] a) throws Exception{
		new OCRTest().downloadExcel();
	}
	
	public void downloadExcel() throws Exception{
		
		//http://localhost:9090/docuserve/document/dates?startDate=2017-06-04&endDate=2017-06-04
			StopWatch sw = new StopWatch();
			
			HttpGet httpPost = new HttpGet("http://localhost:9090/docuserve/document/dates?startDate=2017-06-01&endDate=2017-06-05");
			httpPost.setHeader("Authorization","Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkdW1teUBlbWFpbC5jb20iLCJpYXQiOjE0OTY1ODk3NDksInN1YiI6ImRvY3VzZXJ2ZSIsImlzcyI6ImRvY3VzZXJ2ZSIsImV4cCI6MTQ5NjU5MzM0OX0.VqR0i9uD3Fu60p0dNjrMON-7dpLTddrKrebng0HH7rI");
			HttpClient httpClient = new DefaultHttpClient();
			sw.start();
			HttpResponse response = httpClient.execute(httpPost);
			sw.stop();
			HttpEntity result = response.getEntity();
			IOUtils.copy(result.getContent(), new FileOutputStream("C:/Users/deepthi/Desktop/images/output_images/file.xls"));
		}
		
}
