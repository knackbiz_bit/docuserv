package com.docuserve;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties {

	@Value("${server.directory.file.path}")
    private String serverDirectoryFilePath;

	@Value("${jwt.token.key}")
    private String jwtTokenKey;

	@Value("${jwt.token.expiration.time}")
    private String jwtTokenExpirationTime;
	
	@Value("${docuserve.ocrEnabled}")
    private Boolean ocrEnabled;

	@Value("${docuserve.userIdForExportToExcel}")
    private String userIdForExportToExcel;
	
	@Value("${ocr.remoteUrl}")
	private String remoteUrl;
	
	@Value("${docuserve.reasons}")
	private String reasons;
	
	@Value("${ocr.File.Urls}")
	private String fileUrl;
	
	
	
    public String getServerDirectoryFilePath() {
        return serverDirectoryFilePath;
    }
    
    public String getJwtTokenKey() {
        return jwtTokenKey;
    }
    
    public String getJwtTokenExpirationTime() {
        return jwtTokenExpirationTime;
    }

	public boolean isOCREnabled() {
		return ocrEnabled;
	}

	public String getUserIdForExportToExcel() {
		return userIdForExportToExcel;
	}

	public String getRemoteUrl() {
		return remoteUrl;
	}

	public String getReasons() {
		return reasons;
	}

	public String getFileUrl() {
		return fileUrl;
	}
}