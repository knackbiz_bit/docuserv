package com.docuserve.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.filter.OncePerRequestFilter;

import com.docuserve.security.exception.JwtTokenException;
import com.docuserve.security.model.JwtAuthToken;
import com.docuserve.security.utility.JWTUtility;

public class JwtAuthorizationFilter extends AbstractAuthenticationProcessingFilter 	{
	
//	@Autowired
//	JwtAuthProvider authenticationManager;

	public JwtAuthorizationFilter() {
	        super("/**");
	  }

	/*
	RequestMatcher requestMatcher;

	public JwtAuthorizationFilter(RequestMatcher requestMatcher) {
		this.requestMatcher = requestMatcher;
		//super(requestMatcher);
	}
*/
	/*
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		String authorization = servletRequest.getHeader("Authorization");
		
        if (authorization == null || !authorization.startsWith("Bearer ")) {
        	throw new JwtTokenException("JWT Token either NOT FOUND or INVALID !!");
        }

        JwtAuthToken jwtAuthToken = null;
        String authToken = authorization.substring(7);
		if (authToken != null && authToken.length()>0) {
			jwtAuthToken = new JwtAuthToken(authToken);
			SecurityContextHolder.getContext().setAuthentication(jwtAuthToken);
		}
		return jwtAuthToken;
	}
*/
	/*
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		
		if (!requestMatcher.matches(servletRequest)) {

			String authorization = servletRequest.getHeader("Authorization");
			
	        if (authorization == null || !authorization.startsWith("Bearer ")) {
	        	throw new JwtTokenException("JWT Token either NOT FOUND or INVALID !!");
	        }

	        JwtAuthToken jwtAuthToken = null;
	        String authToken = authorization.substring(7);
			if (authToken != null && authToken.length()>0) {
				jwtAuthToken = new JwtAuthToken(authToken);
				SecurityContextHolder.getContext().setAuthentication(jwtAuthToken);
			}
		}
		chain.doFilter(request, response);
		
	}
*/
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		String header = request.getHeader("Authorization");

        if (header == null || !header.startsWith("Bearer ")) {
            throw new JwtTokenException("No JWT token found in request headers");
        }

        String authToken = header.substring(7);

        JwtAuthToken authRequest = new JwtAuthToken(authToken);

        return getAuthenticationManager().authenticate(authRequest);
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		super.successfulAuthentication(request, response, chain, authResult);
		chain.doFilter(request, response);
	}

	
}
