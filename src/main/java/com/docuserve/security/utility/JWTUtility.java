package com.docuserve.security.utility;

import java.security.Key;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.docuserve.ApplicationProperties;
import com.docuserve.domain.User;
import com.docuserve.logger.DocuserveApiLogger;
import com.docuserve.security.transfer.JwtUserDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JWTUtility {
	
	@Autowired
	ApplicationProperties properties;
	
	@Autowired
	DocuserveApiLogger logger;
	
	public String createToken(User user) {
		
		logger.writeInfo("CReating JWT token for::" + user.getEmailId());
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		 
	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);

	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(properties.getJwtTokenKey());
	    //byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("secretkeyhere");
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
	 
	    JwtBuilder builder = Jwts.builder().setId(user.getEmailId()).setAudience(user.getGroupId().toString())
	                                .setIssuedAt(now)
	                                .setSubject("docuserve")
	                                .setIssuer("docuserve")
	                                .signWith(signatureAlgorithm, signingKey);
	    
	  Date exp = new Date(nowMillis + Long.valueOf(properties.getJwtTokenExpirationTime()));
	  //  Date exp = new Date(nowMillis + 36000000);
	   builder.setExpiration(exp);
	   return builder.compact();
	}
	
	public JwtUserDTO verifyToken(String jwt) {
		
		JwtUserDTO jwtUserDTO = null;
	    try {
			Claims claims = Jwts.parser()         
			   .setSigningKey(DatatypeConverter.parseBase64Binary(properties.getJwtTokenKey()))
			   .parseClaimsJws(jwt).getBody();
			System.out.println("ID: " + claims.getId());
			System.out.println("Subject: " + claims.getSubject());
			System.out.println("Issuer: " + claims.getIssuer());
			System.out.println("Expiration: " + claims.getExpiration());
			System.out.println("groupId: " + claims.getAudience());
			
			jwtUserDTO = new JwtUserDTO(claims.getId(),Long.parseLong(claims.getAudience()));
			
		} catch (JwtException e) {
			e.printStackTrace();
		}
	    return jwtUserDTO;
	}
	
	public static void main(String args[]) {
		
		User user = new User();
		user.setEmailId("dummy@email.com");
		user.setPassword("dummypwd");
		System.out.println(new JWTUtility().createToken(user));
		
		
	}

}
