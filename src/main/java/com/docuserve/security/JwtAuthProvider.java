package com.docuserve.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.docuserve.security.exception.JwtTokenException;
import com.docuserve.security.model.AuthenticatedUser;
import com.docuserve.security.model.JwtAuthToken;
import com.docuserve.security.transfer.JwtUserDTO;
import com.docuserve.security.utility.JWTUtility;

@Component
public class JwtAuthProvider extends AbstractUserDetailsAuthenticationProvider  {
	
	@Autowired
	JWTUtility jwtUtility;
/*
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		try {
			JwtAuthToken jwtToken = (JwtAuthToken)auth;
			JwtUserDTO jwtUserDTO = jwtUtility.verifyToken(jwtToken.getToken());
			if (jwtUserDTO == null) {
				 throw new JwtTokenException("Failed to verify token");
			} else {
				AuthenticatedUser user = new AuthenticatedUser(jwtUserDTO.getUserId());
				return user;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
*/
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }
    
	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {

		JwtAuthToken jwtAuthenticationToken = (JwtAuthToken) authentication;
        String token = jwtAuthenticationToken.getToken();

        JwtUserDTO parsedUser = jwtUtility.verifyToken(token);

        if (parsedUser == null) {
            throw new JwtTokenException("JWT token is not valid");
        }else if(parsedUser.getGroupId()==null || parsedUser.getGroupId().intValue()==0){
        	 throw new JwtTokenException("JWT token is not valid as Group ID/Client ID is not configured with User");
        }
        
        return new AuthenticatedUser(parsedUser.getUserId(), token,parsedUser.getGroupId());
		
	}

}
