package com.docuserve.security.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.docuserve.ApplicationProperties;
import com.docuserve.security.JwtAuthProvider;
import com.docuserve.security.JwtAuthenticationEntryPoint;
import com.docuserve.security.JwtAuthenticationSuccessHandler;
import com.docuserve.security.JwtAuthorizationFilter;
import com.docuserve.security.SkipRequestMatcher;


@Configuration
@EnableWebSecurity
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	public static final String LOGIN_URL = "/users";
	
	public static final String REST_URL= "/**";
	
	@Autowired
	private JwtAuthProvider jwtAuthProvider;

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;
	
	@Value("${jwt.token.allowedurls}")
	private String allowedUrls;

	@Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return new ProviderManager(Arrays.asList(jwtAuthProvider));
    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
        .csrf().disable();
        // All urls must be authenticated (filter for token always fires (/**)
        http
        	.authorizeRequests()
        		.antMatchers(getAllowedUrls().split(",")).permitAll()
        		.anyRequest().authenticated()
        .and()
        // Call our errorHandler if authentication/authorisation fails
        .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
        .and()
        // don't create session
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); //.and()
// 		Custom JWT based security filter
		http
        .addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
	}
	
	@Bean
	public JwtAuthorizationFilter authenticationTokenFilterBean() throws Exception {
		JwtAuthorizationFilter authenticationTokenFilter = new JwtAuthorizationFilter();
		authenticationTokenFilter.setAuthenticationManager(authenticationManager());
		authenticationTokenFilter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
		List<String> pathsToSkip = Arrays.asList(getAllowedUrls().split(","));
		SkipRequestMatcher matcher = new SkipRequestMatcher(pathsToSkip, REST_URL);
		authenticationTokenFilter.setRequiresAuthenticationRequestMatcher(matcher);
		return authenticationTokenFilter;
	}
	
	@Override
	  public void configure(WebSecurity web) throws Exception {
	    web
	      .ignoring()
	      	.antMatchers(getAllowedUrls().split(","));
	}

	public String getAllowedUrls() {
		return allowedUrls;
	}


}
