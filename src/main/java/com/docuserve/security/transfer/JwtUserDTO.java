package com.docuserve.security.transfer;

public class JwtUserDTO {
	
	private String userId;
	
	private Long groupId;

	/**
	 * @param userId
	 */
	public JwtUserDTO(String userId,Long groupId) {
		super();
		this.userId = userId;
		this.groupId=groupId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	

}
