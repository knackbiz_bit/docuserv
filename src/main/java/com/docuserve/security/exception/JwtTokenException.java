package com.docuserve.security.exception;

import org.springframework.security.core.AuthenticationException;

public class JwtTokenException extends AuthenticationException {

	private static final long serialVersionUID = -1764737754549985616L;

	public JwtTokenException(String msg) {
        super(msg);
    }
}
