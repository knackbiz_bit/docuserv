package com.docuserve.domain;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "signatureUrl", "faceUrl", "docType", "ocrOutput" })
public class OcrResponse {

	@JsonProperty("signatureUrl")
	private String signatureUrl;
	@JsonProperty("faceUrl")
	private String faceUrl;
	@JsonProperty("docType")
	private String docType;
	@JsonProperty("ocrOutput")
	private OcrOutput ocrOutput;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("signatureUrl")
	public String getSignatureUrl() {
		return signatureUrl;
	}

	@JsonProperty("signatureUrl")
	public void setSignatureUrl(String signatureUrl) {
		this.signatureUrl = signatureUrl;
	}

	@JsonProperty("faceUrl")
	public String getFaceUrl() {
		return faceUrl;
	}

	@JsonProperty("faceUrl")
	public void setFaceUrl(String faceUrl) {
		this.faceUrl = faceUrl;
	}

	@JsonProperty("ocrOutput")
	public OcrOutput getOcrOutput() {
		return ocrOutput;
	}

	@JsonProperty("ocrOutput")
	public void setOcrOutput(OcrOutput ocrOutput) {
		this.ocrOutput = ocrOutput;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

}