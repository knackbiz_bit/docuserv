package com.docuserve.domain;

import com.docuserve.literals.IfcDocuserveLiterals;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "RTRW", "KELDESA", "NAMA", "GOL", "KECAMATAN", "PEKERJAAN", "BERLAKU", "KEWAR", "JENIS", "NIK",
		"LAHIR","TINGGI", "ALAMAT", "STATUS", "AREA", "DOB", "PROVINCE", "AGAMA", "SIM" })
public class OcrOutput {

	@JsonProperty("RTRW")
	private String rTRW;
	@JsonProperty("KELDESA")
	private String kELDESA;
	@JsonProperty("NAMA")
	private String nAMA;
	@JsonProperty("GOL")
	private String gOL;
	@JsonProperty("KECAMATAN")
	private String kECAMATAN;
	@JsonProperty("PEKERJAAN")
	private String pEKERJAAN;
	@JsonProperty("BERLAKU")
	private String bERLAKU;
	@JsonProperty("KEWAR")
	private String kEWAR;
	@JsonProperty("JENIS")
	private String jENIS;
	@JsonProperty("NIK")
	private String nIK;
	@JsonProperty("LAHIR")
	private String lAHIR;
	@JsonProperty("TINGGI")
	private String tINGGI;
	@JsonProperty("ALAMAT")
	private String aLAMAT;
	@JsonProperty("STATUS")
	private String sTATUS;
	@JsonProperty("AREA")
	private String aREA;
	@JsonProperty("PROVINCE")
	private String pROVINCE;
	@JsonProperty("AGAMA")
	private String aGAMA;
	@JsonProperty("TEMPAT")
	private String tEMPAT;
	@JsonProperty("SIM")
	private String sIM;
	
	@JsonProperty("RTRW")
	public String getRTRW() {
		return rTRW;
	}

	@JsonProperty("RTRW")
	public void setRTRW(String rTRW) {
		this.rTRW = rTRW;
	}

	@JsonProperty("KELDESA")
	public String getKELDESA() {
		return kELDESA;
	}

	@JsonProperty("KELDESA")
	public void setKELDESA(String kELDESA) {
		this.kELDESA = kELDESA;
	}

	@JsonProperty("NAMA")
	public String getNAMA() {
		return nAMA;
	}

	@JsonProperty("NAMA")
	public void setNAMA(String nAMA) {
		this.nAMA = nAMA;
	}

	@JsonProperty("GOL")
	public String getGOL() {
		return gOL;
	}

	@JsonProperty("GOL")
	public void setGOL(String gOL) {
		this.gOL = gOL;
	}

	@JsonProperty("KECAMATAN")
	public String getkECAMATAN() {
		return kECAMATAN;
	}

	@JsonProperty("KECAMATAN")
	public void setkECAMATAN(String kECAMATAN) {
		this.kECAMATAN = kECAMATAN;
	}

	@JsonProperty("PEKERJAAN")
	public String getPEKERJAAN() {
		return pEKERJAAN;
	}

	@JsonProperty("PEKERJAAN")
	public void setPEKERJAAN(String pEKERJAAN) {
		this.pEKERJAAN = pEKERJAAN;
	}

	@JsonProperty("BERLAKU")
	public String getBERLAKU() {
		return bERLAKU;
	}

	@JsonProperty("BERLAKU")
	public void setBERLAKU(String bERLAKU) {
		this.bERLAKU = bERLAKU;
	}

	@JsonProperty("KEWAR")
	public String getKEWAR() {
		return kEWAR;
	}

	@JsonProperty("KEWAR")
	public void setKEWAR(String kEWAR) {
		this.kEWAR = kEWAR;
	}

	@JsonProperty("JENIS")
	public String getJENIS() {
		return jENIS;
	}

	@JsonProperty("JENIS")
	public void setJENIS(String jENIS) {
		this.jENIS = jENIS;
	}

	@JsonProperty("NIK")
	public String getNIK() {
		return nIK;
	}

	@JsonProperty("NIK")
	public void setNIK(String nIK) {
		this.nIK = nIK;
	}

	@JsonProperty("LAHIR")
	public String getLAHIR() {
		return lAHIR;
	}

	@JsonProperty("LAHIR")
	public void setLAHIR(String lAHIR) {
		this.lAHIR = lAHIR;
	}

	@JsonProperty("ALAMAT")
	public String getALAMAT() {
		return aLAMAT;
	}

	@JsonProperty("ALAMAT")
	public void setALAMAT(String aLAMAT) {
		this.aLAMAT = aLAMAT;
	}

	@JsonProperty("STATUS")
	public String getSTATUS() {
		return sTATUS;
	}

	@JsonProperty("STATUS")
	public void setSTATUS(String sTATUS) {
		this.sTATUS = sTATUS;
	}

	@JsonProperty("AREA")
	public String getAREA() {
		return aREA;
	}

	@JsonProperty("AREA")
	public void setAREA(String aREA) {
		this.aREA = aREA;
	}

	@JsonProperty("PROVINCE")
	public String getPROVINCE() {
		return pROVINCE;
	}

	@JsonProperty("PROVINCE")
	public void setPROVINCE(String pROVINCE) {
		this.pROVINCE = pROVINCE;
	}

	@JsonProperty("AGAMA")
	public String getAGAMA() {
		return aGAMA;
	}

	@JsonProperty("AGAMA")
	public void setAGAMA(String aGAMA) {
		this.aGAMA = aGAMA;
	}

	@JsonProperty("TEMPAT")
	public String getTEMPAT() {
		return tEMPAT;
	}

	@JsonProperty("TEMPAT")
	public void setTEMPAT(String tEMPAT) {
		this.tEMPAT = tEMPAT;
	}

	@JsonProperty("TINGGI")
	public String getTINGGI() {
		return tINGGI;
	}

	@JsonProperty("TINGGI")
	public void setTINGGI(String tINGGI) {
		this.tINGGI = tINGGI;
	}

	@JsonProperty("SIM")
	public String getSIM() {
		return sIM;
	}

	@JsonProperty("SIM")
	public void setSIM(String sIM) {
		this.sIM = sIM;
	}
	
	public void toUpperCase() {
		rTRW = rTRW != null ? rTRW.toUpperCase() : "";
		kELDESA = kELDESA != null ? kELDESA.toUpperCase() : "";
		nAMA = nAMA != null ? nAMA.toUpperCase() : "";
		gOL = gOL != null ? gOL.toUpperCase() : "";
		kECAMATAN = kECAMATAN != null ? kECAMATAN.toUpperCase() : "";
		pEKERJAAN = pEKERJAAN != null ? pEKERJAAN.toUpperCase() : "";
		bERLAKU = bERLAKU != null ? bERLAKU.toUpperCase() : "";
		kEWAR = kEWAR != null ? kEWAR.toUpperCase() : "";
		jENIS = jENIS != null ? jENIS.toUpperCase() : "";
		aLAMAT = aLAMAT != null ? aLAMAT.toUpperCase() : "";
		sTATUS = sTATUS != null ? sTATUS.toUpperCase() : "";
		aREA = aREA != null ? aREA.toUpperCase() : "";
		pROVINCE = pROVINCE != null ? pROVINCE.toUpperCase() : "";
		aGAMA = aGAMA != null ? aGAMA.toUpperCase() : "";
		tEMPAT = tEMPAT != null ? tEMPAT.toUpperCase() : "";
		sIM = sIM != null ? sIM.toUpperCase() : "";
	}
	public void removeSpecialCharacters() {
		rTRW = rTRW != null ? rTRW.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		kELDESA = kELDESA != null ? kELDESA.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		nAMA = nAMA != null ? nAMA.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		gOL = gOL != null ? gOL.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		kECAMATAN = kECAMATAN != null ? kECAMATAN.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		pEKERJAAN = pEKERJAAN != null ? pEKERJAAN.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		bERLAKU = bERLAKU != null ? bERLAKU.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		kEWAR = kEWAR != null ? kEWAR.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		jENIS = jENIS != null ? jENIS.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		aLAMAT = aLAMAT != null ? aLAMAT.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		sTATUS = sTATUS != null ? sTATUS.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		aREA = aREA != null ? aREA.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		pROVINCE = pROVINCE != null ? pROVINCE.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		aGAMA = aGAMA != null ? aGAMA.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		tEMPAT = tEMPAT != null ? tEMPAT.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
		sIM = sIM != null ? sIM.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "") : "";
	}
}