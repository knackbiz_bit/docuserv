package com.docuserve.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DOCUSERVE_MASTER")
public class Document implements Serializable{
	
	@Id
	@Column(name = "REQ_ID")
	private String reqId;
	
	@Column(name = "FILE_TYPE")
	private String fileType;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Column(name = "FILE_DATA_JSON")
	private String fileDataJson;
	
	@Column(name = "FILE_URL")
	private String fileUrl;
	
	@Column(name = "OCR_RESPONSE")
	private String ocrResponse;

	@Column(name = "SUBMITTED_BY")
	private String submittedBy;
	
	@Column(name = "FACE_URL")
	private String faceUrl;
	
	@Column(name = "SIGNATURE_URL")
	private String signatureUrl;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SUBMITTED_ON")
	private java.util.Date submittedOn;
	
	@Column(name = "OCR_RESPONSE_IMPROVED")
	private String ocrResponseImproved;

	@Column(name = "DOC_TYPE")
	private String docType;
	
	@Column(name = "CORRCTD_RESPONSE")
	private String correctedResponse;
	
	@Column(name = "REASONS")
	private String reasons;
	
	@Column(name = "IS_RESPONSE_CORRECT")
	private String responseCorrect;
	
	@Column(name = "REQ_ANALYSED")
	private boolean requestAnalysed;
	
	
	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getFileDataJson() {
		return fileDataJson;
	}

	public void setFileDataJson(String fileDataJson) {
		this.fileDataJson = fileDataJson;
	}

	public String getOcrResponse() {
		return ocrResponse;
	}

	public void setOcrResponse(String ocrResponse) {
		this.ocrResponse = ocrResponse;
	}

	public String getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
	}

	public java.util.Date getSubmittedOn() {
		return submittedOn;
	}

	public void setSubmittedOn(java.util.Date submittedOn) {
		this.submittedOn = submittedOn;
	}

	public String getFaceUrl() {
		return faceUrl;
	}

	public void setFaceUrl(String faceUrl) {
		this.faceUrl = faceUrl;
	}

	public String getSignatureUrl() {
		return signatureUrl;
	}

	public void setSignatureUrl(String signatureUrl) {
		this.signatureUrl = signatureUrl;
	}
	public String getOcrResponseImproved() {
		return ocrResponseImproved;
	}

	public void setOcrResponseImproved(String ocrResponseImproved) {
		this.ocrResponseImproved = ocrResponseImproved;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getCorrectedResponse() {
		return correctedResponse;
	}

	public void setCorrectedResponse(String correctedResponse) {
		this.correctedResponse = correctedResponse;
	}

	public String getReasons() {
		return reasons;
	}

	public void setReasons(String reasons) {
		this.reasons = reasons;
	}

	public String getResponseCorrect() {
		return responseCorrect;
	}

	public void setResponseCorrect(String responseCorrect) {
		this.responseCorrect = responseCorrect;
	}

	public boolean isRequestAnalysed() {
		return requestAnalysed;
	}

	public void setRequestAnalysed(boolean requestAnalysed) {
		this.requestAnalysed = requestAnalysed;
	}
	
}