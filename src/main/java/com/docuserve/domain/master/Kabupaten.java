package com.docuserve.domain.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "LEVEL2", schema = "docuserve")
public class Kabupaten implements Serializable {

	private static final long serialVersionUID = -673426199139855619L;

	@Id
	@Column(name = "LEVEL2ID")
	private Long kabupatenID;

	@Column(name = "LEVEL2NAME")
	private String kabupatenName;
	
	@Column(name = "LEVEL1ID")
	private Long provinsiID;
	
	@Column(name = "LEVEL2_TYPE")
	private String level2Flag;

	@Transient
	private List<Kecamatan> kecamatans;

	public Kabupaten() {
		// TODO Auto-generated constructor stub
		this.kecamatans= new ArrayList<Kecamatan>();
	}
	
	public Long getKabupatenID() {
		return kabupatenID;
	}

	public void setKabupatenID(Long kabupatenID) {
		this.kabupatenID = kabupatenID;
	}

	public String getKabupatenName() {
		return kabupatenName;
	}

	public void setKabupatenName(String kabupatenName) {
		this.kabupatenName = kabupatenName;
	}

	public List<Kecamatan> getKecamatans() {
		return kecamatans;
	}

	public void setKecamatans(List<Kecamatan> kecamatans) {
		this.kecamatans = kecamatans;
	}
	@Override
	public String toString() {
		return kabupatenName+"_"+this.kabupatenID;
	}
	

	public Long getProvinsiID() {
		return provinsiID;
	}

	public void setProvinsiID(Long provinsiID) {
		this.provinsiID = provinsiID;
	}

	public String getLevel2Flag() {
		return level2Flag;
	}

	public void setLevel2Flag(String level2Flag) {
		this.level2Flag = level2Flag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kabupatenName == null) ? 0 : kabupatenName.hashCode());
		result = prime * result + ((level2Flag == null) ? 0 : level2Flag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kabupaten other = (Kabupaten) obj;
		if (kabupatenName == null) {
			if (other.kabupatenName != null)
				return false;
		} else if (!kabupatenName.equals(other.kabupatenName))
			return false;
		if (level2Flag == null) {
			if (other.level2Flag != null)
				return false;
		} else if (!level2Flag.equals(other.level2Flag))
			return false;
		return true;
	}
}
