package com.docuserve.domain.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.docuserve.literals.IfcDocuserveLiterals;

@Entity
@Table(name = "LEVEL1", schema = "docuserve")
public class Provinsi implements Serializable {

	private static final long serialVersionUID = 1588048673305392934L;

	@Id
	@Column(name = "LEVEL1ID")
	private Long provinsiID;

	@Column(name = "LEVEL1NAME")
	private String provinceName;

	@Column(name = "COUNTRYID")
	private String countryID;

	@Transient
	private List<Kabupaten> kabupatens;

	public Provinsi() {
		// TODO Auto-generated constructor stub
		this.kabupatens=new ArrayList<Kabupaten>();
		
	}
	
	
	public Long getProvinsiID() {
		return provinsiID;
	}

	public void setProvinsiID(Long provinsiID) {
		this.provinsiID = provinsiID;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCountryID() {
		return countryID;
	}

	public void setCountryID(String countryID) {
		this.countryID = countryID;
	}

	public List<Kabupaten> getKabupatens() {
		return kabupatens;
	}

	public void setKabupatens(List<Kabupaten> kabupatens) {
		this.kabupatens = kabupatens;
	}

	public List<Kabupaten> getKabupatensOnly() {
		return kabupatens.stream().filter(p -> p.getLevel2Flag().equalsIgnoreCase(IfcDocuserveLiterals.KABUPATEN))
				.collect(Collectors.toCollection(() -> new ArrayList<Kabupaten>()));
	} 

	public List<Kabupaten> getKotasOnly() {
		return kabupatens.stream().filter(p -> p.getLevel2Flag().equalsIgnoreCase(IfcDocuserveLiterals.KOTA))
				.collect(Collectors.toCollection(() -> new ArrayList<Kabupaten>()));
	} 

	@Override
	public String toString() {
		return provinceName;
	}
	@Override
	public boolean equals(Object obj) {
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		Provinsi other=(Provinsi)obj;
		if(provinceName==null){
			if(other.provinceName!=null){
				return false;
			}
		}else if(!provinceName.equalsIgnoreCase(other.provinceName)){
			
			return false;
		}
		return true;
	}
	
	
}
