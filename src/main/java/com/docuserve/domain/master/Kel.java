package com.docuserve.domain.master;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LEVEL4", schema = "docuserve")
public class Kel implements Serializable {

	private static final long serialVersionUID = -6752665918846428000L;

	@Id
	@Column(name = "LEVEL4ID")
	private Long kelID;
	
	
	@Column(name = "LEVEL3ID")
	private Long kecamatanID;

	@Column(name = "LEVEL4NAME")
	private String kelName;

	
	public Long getKelID() {
		return kelID;
	}

	public void setKelID(Long kelID) {
		this.kelID = kelID;
	}

	public String getKelName() {
		return kelName;
	}

	public void setKelName(String kelName) {
		this.kelName = kelName;
	}
	
	@Override
	public String toString() {
		return kelName;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		Kel other=(Kel)obj;
		if(kelName==null){
			if(other.kelName!=null){
				return false;
			}
		}else if(!kelName.equalsIgnoreCase(other.kelName)){
			
			return false;
		}
		return true;
	}

	public Long getKecamatanID() {
		return kecamatanID;
	}

	public void setKecamatanID(Long kecamatanID) {
		this.kecamatanID = kecamatanID;
	}
}
