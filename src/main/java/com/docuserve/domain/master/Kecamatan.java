package com.docuserve.domain.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "LEVEL3", schema = "docuserve")
public class Kecamatan implements Serializable {

	private static final long serialVersionUID = -7451650228221085054L;

	@Id
	@Column(name = "LEVEL3ID")
	private Long kecamatanID;

	@Column(name = "LEVEL3NAME")
	private String kecamatanName;

	
	@Column(name = "LEVEL2ID")
	private Long kabupatanID;
	
	@Transient
	private List<Kel> kels;

	
	public Kecamatan() {
		// TODO Auto-generated constructor stub
		this.kels= new ArrayList<Kel>();
	}
	
	public Long getKecamatanID() {
		return kecamatanID;
	}

	public void setKecamatanID(Long kecamatanID) {
		this.kecamatanID = kecamatanID;
	}

	public String getKecamatanName() {
		return kecamatanName;
	}

	public void setKecamatanName(String kecamatanName) {
		this.kecamatanName = kecamatanName;
	}

	public List<Kel> getKels() {
		return kels;
	}

	public void setKels(List<Kel> kels) {
		this.kels = kels;
	}
	
	@Override
	public String toString() {
		return kecamatanName;
	}
	@Override
	public boolean equals(Object obj) {
		if(this==obj){
			return true;
		}
		if(obj==null){
			return false;
		}
		if(getClass()!=obj.getClass()){
			return false;
		}
		Kecamatan other=(Kecamatan)obj;
		if(kecamatanName==null){
			if(other.kecamatanName!=null){
				return false;
			}
		}else if(!kecamatanName.equalsIgnoreCase(other.kecamatanName)){
			
			return false;
		}
		return true;
	}

	public Long getKabupatanID() {
		return kabupatanID;
	}

	public void setKabupatanID(Long kabupatanID) {
		this.kabupatanID = kabupatanID;
	}
}
