
package com.docuserve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import com.google.common.collect.Maps;


@SpringBootApplication
@RestController
@Configuration
@ComponentScan({"com.docuserve"})
public class DocuserveApiApplication {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }
	
	public static void main(String[] args) {

		SpringApplication.run(DocuserveApiApplication.class, args);
		
	}
	
	//@RequestMapping("/")
	public String home(){
		return "This is a Test random number from DB --> " + jdbcTemplate.queryForObject("SELECT RAND()*100;", Maps.newHashMap(), String.class);
	}
}
