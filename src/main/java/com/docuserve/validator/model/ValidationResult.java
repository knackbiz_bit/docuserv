package com.docuserve.validator.model;

public class ValidationResult {
	
	/**
	 * @param fieldName
	 * @param errorMessage
	 */
	public ValidationResult(String fieldName, String errorMessage) {
		super();
		this.fieldName = fieldName;
		this.errorMessage = errorMessage;
	}

	private String fieldName;
	
	private String errorMessage;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	

}
