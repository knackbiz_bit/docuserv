package com.docuserve.validator.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "type",
        "regex",
        "acceptedValues",
        "unAcceptedValues",
        "length"
})
public class OCRValidationRule {

    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;
    @JsonProperty("regex")
    private String regex;
    @JsonProperty("acceptedValues")
    private List<String> acceptedValues;
    @JsonProperty("unAcceptedValues")
    private List<String> unAcceptedValues;
    @JsonProperty("length")
    private int length;
    
    @JsonProperty("name")
	public String getName() {
		return name;
	}
    @JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}
    @JsonProperty("type")
	public String getType() {
		return type;
	}
    @JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}
    @JsonProperty("regex")
	public String getRegex() {
		return regex;
	}
    @JsonProperty("regex")
	public void setRegex(String regex) {
		this.regex = regex;
	}

    @JsonProperty("length")
	public int getLength() {
		return length;
	}
    @JsonProperty("length")
	public void setLength(int length) {
		this.length = length;
	}
	public List<String> getAcceptedValues() {
		return acceptedValues;
	}
	public void setAcceptedValues(List<String> acceptedValues) {
		this.acceptedValues = acceptedValues;
	}
	public List<String> getUnAcceptedValues() {
		return unAcceptedValues;
	}
	public void setUnAcceptedValues(List<String> unAcceptedValues) {
		this.unAcceptedValues = unAcceptedValues;
	}


}
