package com.docuserve.validator.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ocrRules"
})
public class OCRValidationRules {

    @JsonProperty("ocrRules")
    private List<OCRValidationRule> ocrRules = null;

    @JsonProperty("ocrRules")
	public List<OCRValidationRule> getOcrRules() {
		return ocrRules;
	}

    @JsonProperty("ocrRules")
	public void setOcrRules(List<OCRValidationRule> ocrRules) {
		this.ocrRules = ocrRules;
	}

    

}