package com.docuserve.validator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;

import com.docuserve.logger.DocuserveApiLogger;
import com.docuserve.domain.OcrResponse;
import com.docuserve.validator.model.OCRValidationRule;
import com.docuserve.validator.model.OCRValidationRules;
import com.docuserve.validator.model.ValidationResult;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class OCRDataValidator implements Validator {

	@Autowired
	DocuserveApiLogger logger;
	
	@Value("${ocr.validation.rules}")
	String ocrValidationRules;
	
	

	@Override
	public boolean supports(Class<?> ocrClass) {

		System.out.println("Inside supports...");
		return OcrResponse.class.equals(ocrClass);
		
	}

	@Override
	public void validate(Object ocr, Errors error) {
		// validation goes here
		
    	try {
    		MapBindingResult bindingResult = (MapBindingResult)error;
    		BeanWrapper prepareResponseBean = new BeanWrapperImpl((OcrResponse)ocr);

			final ObjectMapper om = new ObjectMapper();
			final OCRValidationRules rules = om.readValue(getOcrValidationRules(), OCRValidationRules.class);
			logger.writeInfo("OCR Validation Starts ::");
			List<ValidationResult> results = new ArrayList<ValidationResult>();
			for (OCRValidationRule ocrRule : rules.getOcrRules()) {
				logger.writeInfo("Validating :: " +  ocrRule.getName() );
				String valueToValidate = (String)prepareResponseBean.getPropertyValue(ocrRule.getName());
				if(StringUtils.isEmpty(valueToValidate)){
					logger.writeInfo("The field Value is empty :: proceeding further \n");
					continue;
				}
				if (ocrRule.getRegex() != null && !valueToValidate.matches(ocrRule.getRegex())) {
					logger.writeInfo("Regex NOT Match for " + ocrRule.getName());
					addError(bindingResult, ocrRule, "REGEX Not Matched");
				}
				if (ocrRule.getAcceptedValues() != null && ocrRule.getAcceptedValues().size()>0 && !ocrRule.getAcceptedValues().contains(valueToValidate)) {
					logger.writeInfo("Does NOT match with Accepted values -" + ocrRule.getName());
					addError(bindingResult, ocrRule, "UNACCEPTABLE Values (Does not matches with Accepted list)");
				}
				if (ocrRule.getUnAcceptedValues() != null && ocrRule.getUnAcceptedValues().size()>0 && ocrRule.getUnAcceptedValues().contains(valueToValidate)) {
					logger.writeInfo("Matches with UNAccepted values -" + ocrRule.getName());
					addError(bindingResult, ocrRule, "UNACCEPTABLE Values (Matches with Unaccepted list)");
				}
				if (ocrRule.getLength() != 0 && (valueToValidate.length() != ocrRule.getLength())) {
					logger.writeInfo("Length NOT expected for - " + ocrRule.getName());
					addError(bindingResult, ocrRule, "LENGTH Mismatch");
				}
				
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	public void addError(MapBindingResult bindingResult, OCRValidationRule ocrRule, String errorMsg) {
		Map errorMap = bindingResult.getTargetMap();
		if (!errorMap.containsKey(ocrRule.getName())) {
			errorMap.put(ocrRule.getName(), new ArrayList<String>());
		}
		((ArrayList)errorMap.get(ocrRule.getName())).add(errorMsg);
		bindingResult.addError(new ObjectError(ocrRule.getName(),errorMsg));
	}
	
	public String getOcrValidationRules() {
		return ocrValidationRules;
	}

	
	
	
}
