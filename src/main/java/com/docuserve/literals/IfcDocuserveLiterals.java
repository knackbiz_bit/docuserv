package com.docuserve.literals;

public class IfcDocuserveLiterals {
	
	public static final String REGEX_SPECIAL_CHAR="[|_+<>~.:]"; 
	public static final String REGEX_SPECIAL_CHAR_FOR_NUMBER="[^\\d]"; 
	public static final String LAKI_LAKI="LAKI-LAKI"; 
	public static final String PEREMPUAN="PEREMPUAN"; 
	public static final String BELUM_KAWIN="BELUM KAWIN"; 
	public static final String KAWIN="KAWIN";
	public static final String CERAI_HIDUP="CERAI HIDUP";
	public static final String SEUMUR_HIDUP="SEUMUR HIDUP";
	public static final String REGEX_SPLIT_STRING_INTO_TWO_CHARACTER="(?<=\\G..)";
	public static final String RTRW="RTRW";
	public static final String KELDESA="KELDESA";
	public static final String GOL="GOL";
	public static final String KECAMATAN="KECAMATAN";
	public static final String KEWAR="KEWAR";
	public static final String AGAMA="AGAMA";
	public static final String AREA="AREA";
	public static final String PROVINCE="PROVINCE";
	public static final String DOB="DOB";
	public static final String NAMA="NAMA";
	public static final String DOC_TYP_FACE="FACE";
	public static final String DOC_TYP_SIG="SIG";
	public static final String DOC_TYP_DL="DL";
	public static final String DOC_TYP_KTP="KTP";
	public static final String SIM="SIM";
	public static final String TINGGI="TINGGI";
	public static final String KOTA="KOTA";
	public static final String KABUPATEN="KABUPATEN";
	public static final String REGEX_FOR_RTRW="[0][0-9][1-9][0][0-9][1-9]";
	public static final int DAYS_MORE_IN_DOB_PEREMPUAN=40;
	public static final String DOC_TYP_TESS="TESS";
	public static final String DOC_TYP_CLEAN="CLEAN";
	public static final String FILE_NAME_CLEAN="clean";
	public static final String FILE_NAME_TESS="tess";
}
