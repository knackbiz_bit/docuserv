package com.docuserve.utility;

import java.util.Date;

public class StringUtility {
	
	public static String formatString(String str){
		return str != null ? str.trim() : "";
	}
	
	public static String convertDateToString(Date date){
		return date != null ? date.toString() : "";
	}
	
}
