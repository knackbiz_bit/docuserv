package com.docuserve.utility;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.text.DateFormat;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;

import com.docuserve.domain.OcrOutput;
import com.docuserve.domain.OcrResponse;
import com.docuserve.dto.DocumentDTO;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ExcelUtility {


    private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);

    private String contentType;

    public ExcelUtility() {
        this.setContentType("application/vnd.ms-excel");
    }

    public final void renderMergedOutputModel(List<DocumentDTO> documentDTOs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Workbook workbook = this.createWorkbook(documentDTOs, request);
        this.buildExcelDocument(documentDTOs, workbook, request, response);
        response.setContentType(this.getContentType());
        this.renderWorkbook(workbook, response);
    }

    protected Workbook createWorkbook(List<DocumentDTO> model, HttpServletRequest request) {
        return new HSSFWorkbook();
    }

    protected void renderWorkbook(Workbook workbook, HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        workbook.write(out);
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    protected void buildExcelDocument(List<DocumentDTO> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\"my-xls-file.xls\"");

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("Document Report");

        // create header row
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("ID");
        header.createCell(1).setCellValue("File Name");
        header.createCell(2).setCellValue("File Url");
        header.createCell(3).setCellValue("Submitted By");
        header.createCell(4).setCellValue("Submitted On");
        header.createCell(5).setCellValue("Document Type");
        header.createCell(6).setCellValue("Signature Url");
        header.createCell(7).setCellValue("Face Url");
        header.createCell(8).setCellValue("PROVINCE");
        header.createCell(9).setCellValue("PROVINCE Validation");
        
        header.createCell(10).setCellValue("AREA");
        header.createCell(11).setCellValue("AREA Validation");
        
        header.createCell(12).setCellValue("NAMA");
        header.createCell(13).setCellValue("NAMA Validation");
        
        header.createCell(14).setCellValue("NIK");
        header.createCell(15).setCellValue("NIK Validation");
        
        header.createCell(16).setCellValue("LAHIR");
        header.createCell(17).setCellValue("LAHIR Validation");
        
        header.createCell(18).setCellValue("JENIS");
        header.createCell(19).setCellValue("JENIS Validation");
        
        header.createCell(20).setCellValue("GOL");
        header.createCell(21).setCellValue("GOL Validation");
        
        header.createCell(22).setCellValue("ALAMAT");
        header.createCell(23).setCellValue("ALAMAT Validation");
        
        header.createCell(24).setCellValue("RTRW");
        header.createCell(25).setCellValue("RTRW Validation");
        
        header.createCell(26).setCellValue("KELDESA");
        header.createCell(27).setCellValue("KELDESA Validation");
        
        header.createCell(28).setCellValue("KECEMATAN");
        header.createCell(29).setCellValue("KECEMATAN Validation");
        
        header.createCell(30).setCellValue("AGAMA");
        header.createCell(31).setCellValue("AGAMA Validation");
        
        header.createCell(32).setCellValue("STATUS");
        header.createCell(33).setCellValue("STATUS Validation");
        
        header.createCell(34).setCellValue("PEKERJAAN");
        header.createCell(35).setCellValue("PEKERJAAN Validation");
        
        header.createCell(36).setCellValue("KEWAR");
        header.createCell(37).setCellValue("KEWAR Validation");
        
        header.createCell(38).setCellValue("BERLAKU");
        header.createCell(39).setCellValue("BERLAKU Validation");
        
        header.createCell(40).setCellValue("TEMPAT");
        header.createCell(41).setCellValue("TEMPAT Validation");
        
        header.createCell(42).setCellValue("TINGGI");
        header.createCell(43).setCellValue("TINGGI Validation");
        
        header.createCell(44).setCellValue("SIM");
        header.createCell(45).setCellValue("SIM Validation");
        
		final ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		OcrResponse ocrRespons = null;
		int rowCount = 1;
		for (DocumentDTO dto : model) {
			Row courseRow = sheet.createRow(rowCount++);
			courseRow.createCell(0).setCellValue(StringUtility.formatString(dto.getReqId()));
			courseRow.createCell(1).setCellValue(StringUtility.formatString(dto.getFileName()));
			courseRow.createCell(2).setCellValue(StringUtility.formatString(dto.getFileUrl()));
			courseRow.createCell(3).setCellValue(StringUtility.formatString(dto.getSubmittedBy()));
			courseRow.createCell(4).setCellValue(StringUtility.convertDateToString(dto.getSubmittedOn()));
			courseRow.createCell(5).setCellValue(StringUtility.formatString(dto.getDocType()));
			if (dto.getOcrResponseImproved() != null) {
				ocrRespons = om.readValue(dto.getOcrResponseImproved(), OcrResponse.class);
				if (ocrRespons.getOcrOutput() != null) {
					courseRow.createCell(6).setCellValue(StringUtility.formatString(dto.getSignatureUrl()));
					courseRow.createCell(7).setCellValue(StringUtility.formatString(dto.getFaceUrl()));
					courseRow.createCell(8)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getPROVINCE()));
					courseRow.createCell(10)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getAREA()));
					courseRow.createCell(12)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getNAMA()));
					courseRow.createCell(14)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getNIK()));
					courseRow.createCell(16)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getLAHIR()));
					courseRow.createCell(18)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getJENIS()));
					courseRow.createCell(20)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getGOL()));
					courseRow.createCell(22)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getALAMAT()));
					courseRow.createCell(24)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getRTRW()));
					courseRow.createCell(26)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getKELDESA()));
					courseRow.createCell(28)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getkECAMATAN()));
					courseRow.createCell(30)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getAGAMA()));
					courseRow.createCell(32)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getSTATUS()));
					courseRow.createCell(34)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getPEKERJAAN()));
					courseRow.createCell(36)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getKEWAR()));
					courseRow.createCell(38)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getBERLAKU()));
					courseRow.createCell(40)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getTEMPAT()));
					courseRow.createCell(42)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getTINGGI()));
					courseRow.createCell(44)
							.setCellValue(StringUtility.formatString(ocrRespons.getOcrOutput().getSIM()));

				}
			}

		}
    }
}