package com.docuserve.response;

public class DocumentUrl {

	private String documentType;
	
	private String url;

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @param documentType
	 * @param url
	 * 
	 */
	
	public DocumentUrl() {
		// TODO Auto-generated constructor stub
	}
	public DocumentUrl(String documentType, String url) {
		super();
		this.documentType = documentType;
		this.url = url;
	}
}
