package com.docuserve.response;

public class OCRResponse {

	// province
	private String provinsi;
	// kabupaten
	private String kabupaten;
	// Number	
	private String nik;
    
	//Name
    private String nama;
	
	// City
    private String tempat;
    
	//DOB
    private String tgl;
    
    // jenis
    private String jenis;
    
	// Blood Group
    private String goldarah;
    
	//Address
    private String alamat;
    // Address 1
    private String rtrw;
    // Address 2
    private String desa;
    // Address 3
    private String kecamatan;
    
	//Religion
    private String agama;
    // status
    private String status;
    // pekerjaan
    private String pekerjaan;
	// kewar
    private String kewar;
    
    //berlaku
    private String berlaku;

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTempat() {
		return tempat;
	}

	public void setTempat(String tempat) {
		this.tempat = tempat;
	}

	public String getTgl() {
		return tgl;
	}

	public void setTgl(String tgl) {
		this.tgl = tgl;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public String getGoldarah() {
		return goldarah;
	}

	public void setGoldarah(String goldarah) {
		this.goldarah = goldarah;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getDesa() {
		return desa;
	}

	public void setDesa(String desa) {
		this.desa = desa;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPekerjaan() {
		return pekerjaan;
	}

	public void setPekerjaan(String pekerjaan) {
		this.pekerjaan = pekerjaan;
	}

	public String getKewar() {
		return kewar;
	}

	public void setKewar(String kewar) {
		this.kewar = kewar;
	}

	public String getBerlaku() {
		return berlaku;
	}

	public void setBerlaku(String berlaku) {
		this.berlaku = berlaku;
	}

	public String getRtrw() {
		return rtrw;
	}

	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getKabupaten() {
		return kabupaten;
	}

	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}

	@Override
	public String toString() {
		return "OCRResponse [provinsi=" + provinsi + ", kabupaten=" + kabupaten + ", nik=" + nik + ", nama=" + nama
				+ ", tempat=" + tempat + ", tgl=" + tgl + ", jenis=" + jenis + ", goldarah=" + goldarah + ", alamat="
				+ alamat + ", rtrw=" + rtrw + ", desa=" + desa + ", kecamatan=" + kecamatan + ", agama=" + agama
				+ ", status=" + status + ", pekerjaan=" + pekerjaan + ", kewar=" + kewar + ", berlaku=" + berlaku + "]";
	}
    
	
}
