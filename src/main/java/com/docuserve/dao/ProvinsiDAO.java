package com.docuserve.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.docuserve.domain.master.Provinsi;

@Repository(value="provinsiDAO")
public interface ProvinsiDAO extends CrudRepository<Provinsi, Long> {

}
