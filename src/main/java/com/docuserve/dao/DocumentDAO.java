package com.docuserve.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.docuserve.domain.Document;

@Repository(value="documentDAO")
public interface DocumentDAO extends CrudRepository<Document, String> {
	
	@Transactional
	@Query("select c from Document c where c.submittedOn > :startDate AND c.submittedOn < :endDate AND c.submittedBy in :userIds")
    List<Document> findDocuments(@Param("startDate")Date startDate, @Param("endDate")Date endDate,@Param("userIds") List<String> userIds);
	
	@Transactional
	@Query("select c from Document c where c.submittedBy != :userId" )
    List<Document> findDocuments(@Param("userId")String userId);
	
	@Transactional
    List<Document> findTop15BySubmittedByInOrderBySubmittedOnDesc(@Param("userIds") List<String> userIds);
	
	@Transactional
    List<Document> findBySubmittedByInOrderBySubmittedOnDesc(@Param("userIds") List<String> userIds,Pageable pageable);

}
