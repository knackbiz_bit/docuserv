package com.docuserve.dao;

import org.springframework.data.repository.CrudRepository;

import com.docuserve.domain.master.Kecamatan;

public interface KecamatanDAO extends CrudRepository<Kecamatan, Long> {

}
