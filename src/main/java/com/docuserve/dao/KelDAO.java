package com.docuserve.dao;

import org.springframework.data.repository.CrudRepository;

import com.docuserve.domain.master.Kel;

public interface KelDAO extends CrudRepository<Kel, Long> {

}
