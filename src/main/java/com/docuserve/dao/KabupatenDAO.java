package com.docuserve.dao;

import org.springframework.data.repository.CrudRepository;

import com.docuserve.domain.master.Kabupaten;

public interface KabupatenDAO extends CrudRepository<Kabupaten, Long> {

}
