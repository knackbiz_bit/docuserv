package com.docuserve.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.docuserve.domain.User;

@Repository(value="userDAO")
public interface UserDAO extends CrudRepository<User, String> {
	
	@Transactional
    List<User> findUsersByGroupId(@Param("groupId")Long groupId);

}
