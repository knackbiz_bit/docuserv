package com.docuserve.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.docuserve.ApplicationProperties;
import com.docuserve.dao.DocumentDAO;
import com.docuserve.dao.UserDAO;
import com.docuserve.domain.Document;
import com.docuserve.domain.OcrOutput;
import com.docuserve.domain.OcrResponse;
import com.docuserve.domain.User;
import com.docuserve.domain.master.Kabupaten;
import com.docuserve.domain.master.Kecamatan;
import com.docuserve.domain.master.Provinsi;
import com.docuserve.dto.DocumentDTO;
import com.docuserve.dto.DocumentResponseDTO;
import com.docuserve.exceptionHandling.DocuserveException;
import com.docuserve.literals.IfcDocuserveLiterals;
import com.docuserve.logger.DocuserveApiLogger;
import com.docuserve.mapper.DocumentMapper;
import com.docuserve.mapper.OCRResponseMapper;
import com.docuserve.response.DocumentUrl;
import com.docuserve.utility.StringUtility;
import com.docuserve.validator.OCRDataValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service(value="documentService")
public class DocumentService {
	
	@Autowired
	DocumentDAO documentDAO;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	OcrIntegrationService ocrIntegrationService;
	
	@Autowired
	GeoPoliticalCacheService geoPoliticalCacheService;
	
	@Autowired
	DocumentMapper documentMapper;
	
	@Autowired
	OCRResponseMapper ocrResponseMapper;
	
	@Autowired
	ApplicationProperties properties;
	
	@Autowired
	OCRDataValidator ocrDataValidator;
	
	@Autowired
	private NameComparisonService nameComparisonService;
	
	@Autowired
	DocuserveApiLogger LOGGER;
	
	@Transactional
	public DocumentResponse save(String documentJson, MultipartFile file, String userId,Long groupId) throws Exception{
		
		LOGGER.writeInfo("Service Layer START::" + documentJson);
		
		Document document = new Document();
		
		documentMapper.mapDomain(document, documentJson, file, userId);
		LOGGER.writeInfo("Assigned unique REQUEST ID:: " + document.getReqId());
		String fileUrl = saveFileOnLocalDisk(document.getReqId(), file,groupId);
		
		String ocrData = getOcrData(file, document.getReqId(), groupId);
		
		System.out.println(ocrData);
		
		try {
			
			final ObjectMapper om = new ObjectMapper();
			om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
			OcrResponse ocrRespons = om.readValue(ocrData, OcrResponse.class);
			
			ocrResponseMapper.prepareOcrOutput(ocrRespons.getOcrOutput(),getAllProvinces());
			
			/* Document Domain */
			document.setOcrResponse(ocrData);
			document.setFileUrl(fileUrl);
			document.setFaceUrl(ocrRespons.getFaceUrl());
			document.setSignatureUrl(ocrRespons.getSignatureUrl());
			document.setDocType(ocrRespons.getDocType());
			document.setOcrResponseImproved(om.writeValueAsString(ocrRespons));
			document.setCorrectedResponse(om.writeValueAsString(ocrRespons));//By default corrected would be the response improved
			document.setRequestAnalysed(false);
			documentDAO.save(document);
			
			
			/* Document Response*/
			DocumentResponse response = new DocumentResponse();
			
			checkUserInputs(documentJson, om, ocrRespons, response);
			
			if (ocrRespons.getFaceUrl()!=null ) response.getDocumentUrls().add(new DocumentUrl("Face URL", "face_"+document.getReqId()));
			if (ocrRespons.getSignatureUrl()!=null )response.getDocumentUrls().add(new DocumentUrl("Signature URL", "sig_"+ocrRespons.getSignatureUrl()));
			Map<String, String> ocrMap = om.convertValue(ocrRespons.getOcrOutput(), Map.class);
			response.setFields(filterOcrMap(ocrMap,ocrRespons.getDocType()));
			response.setContentId(document.getReqId());
			response.setDocumentType(document.getFileType());
			response.setSuccess(true);
			
			return response;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new DocuserveException("Exception while processing converted text from image");
		}
	}

	private Map<String, String> filterOcrMap(Map<String, String> ocrMap, String docType) {
		
		if(IfcDocuserveLiterals.DOC_TYP_DL.equals(docType)){
			ocrMap.remove(IfcDocuserveLiterals.RTRW);
			ocrMap.remove(IfcDocuserveLiterals.KELDESA);
			ocrMap.remove(IfcDocuserveLiterals.GOL);
			ocrMap.remove(IfcDocuserveLiterals.KECAMATAN);
			ocrMap.remove(IfcDocuserveLiterals.KEWAR);
			ocrMap.remove(IfcDocuserveLiterals.AGAMA);				
			ocrMap.remove(IfcDocuserveLiterals.AREA);				
			ocrMap.remove(IfcDocuserveLiterals.PROVINCE);				
		}else{
			if(ocrMap.containsKey(IfcDocuserveLiterals.SIM))ocrMap.remove(IfcDocuserveLiterals.SIM);
			ocrMap.remove(IfcDocuserveLiterals.TINGGI);
		}
		
		return ocrMap;
	}

	private String getOcrData(MultipartFile file, String reqId,Long groupId) throws Exception, IOException {
		
		// Once we have OCR details, necessary parameters will be passed to OCR
		String ocrData = "";
        if(properties.isOCREnabled()){
			ocrData = ocrIntegrationService.getResponse(reqId, file.getInputStream(),groupId);
			
			LOGGER.writeInfo("OCR response for REQ --> " + reqId + " is " + ocrData);
		}
		if (ocrData.isEmpty()) {
			ocrData = "{\"signatureUrl\":\"\",\"faceUrl\":\"\",\"ocrOutput\":{\"RTRW\":\"RTRW=001004\",\"KELDESA\":\"KELDESA=AROMBU\",\"NAMA\":\"NAMA=YUSRIN, SH\",\"GOL\":\"GOL=Darah\",\"KECEMATAN\":\"KECEMATAN=UNAAHA\",\"PEKERJAAN\":\"PEKERJAAN=SIPIL (PNS)\",\"BERLAKU\":\"BERLAKU=SEUMUR HIDUP\",\"KEWAR\":\"KEWAR=wit\",\"LAKI\":\"LAKI=LAKI-LAK1-\",\"NIK\":\"nik= 7?402022401820006\",\"LAHIR\":\"LAHIR=Lahe\",\"ALAMAT\":\"ALAMAT=Alamat KEL AROMBI\",\"STATUS\":\"STATUS=KAWIN\",\"AREA\":\"AREA=KONAWE\",\"DOB\":\"DOB=24-01.-\",\"PROVINCE\":\"PROVINCE=SULAWES! TENGG\",\"AGAMA\":\"AGAMA='-  IS\",\"JENIS\":\"JENIS=\"}}";
			//ocrData = "{\"signatureUrl\":\"URL here\"}";
		}
		return ocrData;
	}

	private void checkUserInputs(String documentJson, final ObjectMapper om, OcrResponse ocrRespons, DocumentResponse response) throws IOException, JsonProcessingException {
		
		JsonNode rootNode = om.readTree(documentJson);
		response.setDataMatchStatus(new HashMap<String,Boolean>());
		
		String userDOB = rootNode.path(IfcDocuserveLiterals.DOB)==null || StringUtils.isBlank(rootNode.path(IfcDocuserveLiterals.DOB).asText()) ? "" : rootNode.path(IfcDocuserveLiterals.DOB).asText();
		String userName = rootNode.path(IfcDocuserveLiterals.NAMA)==null || StringUtils.isBlank(rootNode.path(IfcDocuserveLiterals.NAMA).asText()) ? "" : rootNode.path(IfcDocuserveLiterals.NAMA).asText();
		
		boolean isNameMatching = nameComparisonService.isMatching(userName, ocrRespons.getOcrOutput().getNAMA());
		response.getDataMatchStatus().put(IfcDocuserveLiterals.NAMA, isNameMatching);
		
		String closestMatch = ocrRespons.getOcrOutput().getLAHIR();
		if(!StringUtils.isEmpty(closestMatch)){
			boolean isDOBMatching  = userDOB.replaceAll("-", "").equalsIgnoreCase(closestMatch.replaceAll("-", ""));
			response.getDataMatchStatus().put(IfcDocuserveLiterals.DOB, isDOBMatching);
		}
	}
	
	private String saveFileOnLocalDisk(String fileName, MultipartFile file, Long groupId) throws Exception{
		try {
			LOGGER.writeInfo("Save file in directory structure::");
			//save file in directory structure
			String rootDirectory=properties.getServerDirectoryFilePath()+groupId;
			if(!new File(rootDirectory).exists())
			new File(rootDirectory).mkdir();
			String dateBasedFolderString = rootDirectory +File.separator+ getTodaysDate();
			/*Check if this folder exists, else create one*/
			File dateBasedFolder = new File(dateBasedFolderString);
			if(!dateBasedFolder.exists()){
				dateBasedFolder.mkdir();
			}
			String fileOnServerPath = dateBasedFolder +File.separator + fileName + "_" + file.getOriginalFilename();
			IOUtils.copy(file.getInputStream(), new FileOutputStream(new File(fileOnServerPath)));
			return fileOnServerPath;
		
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.writeError(exception);
			throw new DocuserveException("Exception while saving file to the disk ");
		}
	}
	
	private String getTodaysDate() {
		DateFormat formatter = new SimpleDateFormat("dd_MM_yyyy");
		Date today = new Date();
		return formatter.format(today);
	}
	
	public List<DocumentDTO> getAllDocuments() throws Exception{
		
		try {
			
			List<Document> documents = (List<Document>) documentDAO.findAll();
			return documentMapper.mapDomainToDTO(documents);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while getting data from database ");
		}
	}

	public List<DocumentDTO> getDocuments(final String userId) throws Exception{
		
		try {
			
			List<Document> documents = (List<Document>) documentDAO.findDocuments(userId);
			return documentMapper.mapDomainToDTO(documents);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while getting data from database ");
		}
	}

	public List<DocumentDTO> getDocuments(final Date startDate, final Date endDate, Long groupId) throws Exception{
		
		try {
		    //Get All Users Ids for a group Id
			List<User>userList=userDAO.findUsersByGroupId(groupId);
			List<String> userIds= new ArrayList<String>();
			for(User user :userList){
				if(!userIds.contains(user.getEmailId()))
				userIds.add(user.getEmailId());
			}
			List<Document> documents = (List<Document>) documentDAO.findDocuments(startDate, endDate,userIds);
			return documentMapper.mapDomainToDTO(documents);
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while getting data from database ");
		}
	}
	
	public List<Provinsi> getAllProvinces() throws Exception{
		
		List<Provinsi> provinsi = null;
		
		try {
			
			provinsi = geoPoliticalCacheService.getAllProvinsies();
			
			if (provinsi.isEmpty()){
				
				geoPoliticalCacheService.loadProvinsiCache();
				provinsi = geoPoliticalCacheService.getAllProvinsies();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while getting provinsi information");
		}
		
		return provinsi;
		
	}

	public InputStream downloadDocument(String fileUrl, String type, HttpServletResponse response) throws Exception{
		
		try {
			File file = null;
			if(IfcDocuserveLiterals.DOC_TYP_CLEAN.equalsIgnoreCase(type)){
				
				LOGGER.writeInfo("Clean File URL " + properties.getFileUrl()+File.separator+fileUrl+File.separator+IfcDocuserveLiterals.FILE_NAME_CLEAN);
				
				file = new File(properties.getFileUrl()+File.separator+fileUrl+File.separator+IfcDocuserveLiterals.FILE_NAME_CLEAN);
				
			}
			else if(IfcDocuserveLiterals.DOC_TYP_TESS.equalsIgnoreCase(type)){
				
				LOGGER.writeInfo("Tess File URL " + properties.getFileUrl()+File.separator+fileUrl+File.separator+IfcDocuserveLiterals.FILE_NAME_TESS);
				
				file = new File(properties.getFileUrl()+File.separator+fileUrl+File.separator+IfcDocuserveLiterals.FILE_NAME_TESS);
				
			}
			else{
				Document doc = documentDAO.findOne(fileUrl);
				
				LOGGER.writeInfo("Found Document for this URL :: " + fileUrl + " ---> " + doc);
				
				if (IfcDocuserveLiterals.DOC_TYP_FACE.equalsIgnoreCase(type)) {
					
					LOGGER.writeInfo("Face URL " + doc.getFaceUrl() + " ---> " + doc);
				
					file = new File(doc.getFaceUrl());
				
				} else if (IfcDocuserveLiterals.DOC_TYP_SIG.equalsIgnoreCase(type)) {
					
					LOGGER.writeInfo("SIG URL " + doc.getSignatureUrl() + " ---> "  + doc);
					
					file = new File(doc.getSignatureUrl());
				
				} /*else if(IfcDocuserveLiterals.DOC_TYP_KTP.equalsIgnoreCase(type)) {
					LOGGER.writeInfo("KTP URL>>>>"+doc.getFileUrl());
					if(doc.getFileUrl().contains("C:\\temp\\docfile\\")){
						doc.setFileUrl(doc.getFileUrl().replace("C:\\temp\\docfile\\", "C\\:\\\\temp\\\\docfile\\\\"));
						LOGGER.writeInfo("KTP URL after adding \\\\ >>>>"+doc.getFileUrl());
					}
					file = new File(doc.getFileUrl());
				}*/else{
					LOGGER.writeInfo("File URL>>>>"+doc.getFileUrl());
					file = new File(doc.getFileUrl());
				}
				
				if (doc.getFileType() != null)
					response.setContentType(doc.getFileType());
				
				if (doc.getFileName() != null)
					response.setHeader("Content-disposition", "attachment; filename=" + doc.getFileName());
			}
			return new FileInputStream(file);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while getting downloading File");
			
		}
		
	}
	
	public String getTemporaryOcrDataFromFile() {
		try {
			return org.apache.commons.io.IOUtils.toString( new FileInputStream("E:\\renewed\\knack\\freelancing\\Appcleus\\OCR validation\\samples\\samples\\sample.tess"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Empty";
	}
	
	public String  validateNIK(OcrOutput ocrOutput) {
		
		if(ocrOutput.getNIK()!=null && ocrOutput.getNIK().length()==16){
		 String []nIKArray=ocrOutput.getNIK().split("(?<=\\G..)");
		 //validate Province
		 Provinsi province=geoPoliticalCacheService.getProvinceFromCache(ocrOutput.getPROVINCE());
		 if(province!=null && province.getProvinsiID()!=Long.parseLong(nIKArray[0])){
			return "PROVINCE is not matching with NIK information";
		 }
		 //validate Regency
		 String kabupatenType =findAreaType(ocrOutput);
		 Kabupaten kabupaten=geoPoliticalCacheService.getKabupatanFromCache(ocrOutput.getAREA()+"_"+kabupatenType);
		 if(kabupaten!=null && kabupaten.getKabupatenID()!=Long.parseLong(nIKArray[1])){
			return "AREA is not matching with NIK information";
		 }
		 //KECAMATAN distict
		 Kecamatan kecamatan=geoPoliticalCacheService.getKecamatanFromCache(ocrOutput.getkECAMATAN());
		 if(kecamatan!=null && kecamatan.getKecamatanID()!=Long.parseLong(nIKArray[2])){
			 return "KECAMATAN is not matching with NIK information";
		 }
		 //validate DOB
		 //Which one is DOB fields
		 /*if("LAKI-LAKI".equalsIgnoreCase(ocrOutput.getJENIS())){
			 String dateOfBirth=nIKArray[3];
			 String monthOfBirth=nIKArray[4];
			 String yearOfBirth=nIKArray[5];
			 if(true)
			 return "DOB is not matching with NIK information";
			
		 }else{
			 String dateOfBirth=nIKArray[3];//-40 for woman
			 String monthOfBirth=nIKArray[4];
			 String yearOfBirth=nIKArray[5];
			 if(true)
			 return "DOB is not matching with NIK information";
		 }*/
		 //String serialNumber=nIKArray[6]+nIKArray[7];
		}
		return null;
	}
	
	private String findAreaType(OcrOutput ocr) {
		Pattern kotaPattern = Pattern.compile("[Kk][Oo][TtIl][A]");
		Pattern kabuPatenPattern = Pattern.compile("[Kk][A][B8][U0O]");
		Matcher matcher = kotaPattern.matcher(ocr.getAREA());
		boolean matches = matcher.lookingAt();
		if (!matches) {
			matcher = kabuPatenPattern.matcher(ocr.getAREA());
			matches = matcher.lookingAt();
			if (!matches) {
				return IfcDocuserveLiterals.KOTA; //when no match found KOTA is sent
			} else {
				ocr.setAREA(matcher.replaceFirst(""));
				return IfcDocuserveLiterals.KABUPATEN; 
			}
		} else {
			ocr.setAREA(matcher.replaceFirst(""));
			return IfcDocuserveLiterals.KOTA;
		}
		
	}
	
  public DocumentResponseDTO fetchDocument(String reqId) throws Exception{
		
		try {
			Document document = (Document) documentDAO.findOne(reqId);
			final ObjectMapper om = new ObjectMapper();
			om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
			String ocrResponseStr=document.getCorrectedResponse();
			if(StringUtils.isEmpty(ocrResponseStr))
			ocrResponseStr=document.getOcrResponseImproved();
			OcrResponse ocrRespons = om.readValue(ocrResponseStr, OcrResponse.class);
			/* Document Response*/
			DocumentResponseDTO responseDto = new DocumentResponseDTO();
			
			if (ocrRespons.getFaceUrl()!=null ) responseDto.getDocumentUrls().add(new DocumentUrl("Face URL", "face_"+document.getReqId()));
			if (ocrRespons.getSignatureUrl()!=null )responseDto.getDocumentUrls().add(new DocumentUrl("Signature URL", "sig_"+ocrRespons.getSignatureUrl()));
			if (document.getFileUrl()!=null )responseDto.getDocumentUrls().add(new DocumentUrl("KTP URL", "file_"+document.getFileUrl()));
			responseDto.getDocumentUrls().add(new DocumentUrl("Clean URL", "clean_"+document.getReqId()));
			responseDto.getDocumentUrls().add(new DocumentUrl("Tess URL", "tess_"+document.getReqId()));
			Map<String, String> ocrMap = om.convertValue(ocrRespons.getOcrOutput(), Map.class);
			responseDto.setFields(filterOcrMap(ocrMap,ocrRespons.getDocType()));
			if(StringUtils.isNotEmpty(document.getReasons()))
			responseDto.setReasons(om.readValue(document.getReasons(), new TypeReference<Map<String, String>>(){}));
			if(StringUtils.isNotEmpty(document.getResponseCorrect()))
			responseDto.setResponseCorrect(om.readValue(document.getResponseCorrect(), new TypeReference<Map<String, String>>(){}));
			responseDto.setRequestAnalysed(document.isRequestAnalysed());
			responseDto.setContentId(document.getReqId());
			responseDto.setDocumentType(document.getFileType());
			responseDto.setSuccess(true);
			responseDto.setSubmittedOn(StringUtility.convertDateToString(document.getSubmittedOn()));
			return responseDto;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while getting data from database ");
		}
	}
  
   public List<DocumentResponseDTO> fetchDocuments(Long groupId, int pageNumber) throws Exception{
		
		try {
			List<DocumentResponseDTO> documentResponses= new ArrayList<DocumentResponseDTO>();
			//groupId
			List<User> users=userDAO.findUsersByGroupId(groupId);
			List<String> userIds=new ArrayList<String>();
			for(User user :users){
				userIds.add(user.getEmailId());
			}
			Pageable pageable = new PageRequest(pageNumber, 15);
			List<Document> documents = documentDAO.findBySubmittedByInOrderBySubmittedOnDesc(userIds, pageable);
			for(Document document : documents){
				final ObjectMapper om = new ObjectMapper();
				om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
				String ocrResponseStr=document.getCorrectedResponse();
				if(StringUtils.isEmpty(ocrResponseStr))
				ocrResponseStr=document.getOcrResponseImproved();
				OcrResponse ocrRespons = om.readValue(ocrResponseStr, OcrResponse.class);
				/* Document Response*/
				DocumentResponseDTO responseDto = new DocumentResponseDTO();
				
				if (ocrRespons.getFaceUrl()!=null ) responseDto.getDocumentUrls().add(new DocumentUrl("Face URL", "face_"+document.getReqId()));
				if (ocrRespons.getSignatureUrl()!=null )responseDto.getDocumentUrls().add(new DocumentUrl("Signature URL", "sig_"+ocrRespons.getSignatureUrl()));
				if (document.getFileUrl()!=null )responseDto.getDocumentUrls().add(new DocumentUrl("KTP URL", "file_"+document.getFileUrl()));
				responseDto.getDocumentUrls().add(new DocumentUrl("Clean URL", "clean_"+document.getReqId()));
				responseDto.getDocumentUrls().add(new DocumentUrl("Tess URL", "tess_"+document.getReqId()));
				Map<String, String> ocrMap = om.convertValue(ocrRespons.getOcrOutput(), Map.class);
				responseDto.setFields(filterOcrMap(ocrMap,ocrRespons.getDocType()));
				if(StringUtils.isNotEmpty(document.getReasons()))
				responseDto.setReasons(om.readValue(document.getReasons(), new TypeReference<Map<String, String>>(){}));
				if(StringUtils.isNotEmpty(document.getResponseCorrect()))
				responseDto.setResponseCorrect(om.readValue(document.getResponseCorrect(), new TypeReference<Map<String, String>>(){}));
				responseDto.setRequestAnalysed(document.isRequestAnalysed());
				responseDto.setContentId(document.getReqId());
				responseDto.setDocumentType(document.getFileType());
				responseDto.setSuccess(true);
				responseDto.setSubmittedOn(StringUtility.convertDateToString(document.getSubmittedOn()));
				documentResponses.add(responseDto);
			}
			return documentResponses;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while getting data from database ");
		}
	}
   
   public DocumentResponseDTO updateDocument(DocumentResponseDTO documentResponseDTO) throws Exception{
		
		try {
				final ObjectMapper om = new ObjectMapper();
				om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
			    Document document = documentDAO.findOne(documentResponseDTO.getContentId());
			    document.setReasons(om.writeValueAsString(documentResponseDTO.getReasons()));
			    document.setResponseCorrect(om.writeValueAsString(documentResponseDTO.getResponseCorrect()));
			    //update the fields only
			    String ocrResponseStr=document.getCorrectedResponse();
			    if(StringUtils.isEmpty(ocrResponseStr))
				ocrResponseStr=document.getOcrResponseImproved();
				OcrResponse ocrRespons = om.readValue(ocrResponseStr, OcrResponse.class);
				OcrOutput ocrOutput = om.readValue(om.writeValueAsString(documentResponseDTO.getFields()), OcrOutput.class);   
				ocrRespons.setOcrOutput(ocrOutput);
				document.setCorrectedResponse(om.writeValueAsString(ocrRespons));
				//check the request status, if all 19 fields are set to either yes or no then status should be analysed.
				if(documentResponseDTO.getResponseCorrect()!=null){
					String fields[]="NIK,RTRW,KELDESA,NAMA,JENIS,PEKERJAAN,KECAMATAN,BERLAKU,KEWAR,LAHIR,ALAMAT,STATUS,AREA,PROVINCE,AGAMA,TEMPAT,GOL,FACE,SIGN".split(",");
					document.setRequestAnalysed(true);
					for(String field : fields){
						if(!documentResponseDTO.getResponseCorrect().containsKey(field)){
							document.setRequestAnalysed(false);
							break;
						}
					}
				}
			    documentDAO.save(document);
				/* Document Response*/
				DocumentResponseDTO responseDto = new DocumentResponseDTO();
				
				if (ocrRespons.getFaceUrl()!=null ) responseDto.getDocumentUrls().add(new DocumentUrl("Face URL", "face_"+document.getReqId()));
				if (ocrRespons.getSignatureUrl()!=null )responseDto.getDocumentUrls().add(new DocumentUrl("Signature URL", "sig_"+ocrRespons.getSignatureUrl()));
				if (document.getFileUrl()!=null )responseDto.getDocumentUrls().add(new DocumentUrl("KTP URL", "file_"+document.getFileUrl()));
				responseDto.getDocumentUrls().add(new DocumentUrl("Clean URL", "clean_"+document.getReqId()));
				responseDto.getDocumentUrls().add(new DocumentUrl("Tess URL", "tess_"+document.getReqId()));
				Map<String, String> ocrMap = om.convertValue(ocrRespons.getOcrOutput(), Map.class);
				responseDto.setFields(filterOcrMap(ocrMap,ocrRespons.getDocType()));
				if(document.getReasons()!=null)
				responseDto.setReasons(om.readValue(document.getReasons(), new TypeReference<Map<String, String>>(){}));
				if(document.getResponseCorrect()!=null)
				responseDto.setResponseCorrect(om.readValue(document.getResponseCorrect(), new TypeReference<Map<String, String>>(){}));
				responseDto.setRequestAnalysed(document.isRequestAnalysed());
				responseDto.setContentId(document.getReqId());
				responseDto.setDocumentType(document.getFileType());
				responseDto.setSuccess(true);
				responseDto.setSubmittedOn(StringUtility.convertDateToString(document.getSubmittedOn()));
		    return responseDto;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while updating data to database ");
		}
	}
	
}
