package com.docuserve.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.docuserve.dao.UserDAO;
import com.docuserve.domain.User;
import com.docuserve.dto.UserDTO;
import com.docuserve.mapper.UserMapper;
import com.docuserve.response.UserResponse;
import com.docuserve.security.utility.JWTUtility;



@Service(value="userService")
public class UserService {

	@Autowired
	UserDAO userDAO;

	@Autowired
	UserMapper userMapper;
	
	@Autowired
	JWTUtility jwtUtility;
	
	@Transactional
	public UserResponse login(UserDTO userDto) throws Exception{
		
		User user = new User();
		userMapper.mapDomain(user, userDto);
		
		UserResponse userResponse = new UserResponse();
		userResponse.setId(user.getEmailId());
		User userDomain=userDAO.findOne(user.getEmailId());
		if(user.equals(userDomain)) {
			userResponse.setToken(jwtUtility.createToken(userDomain));
			userResponse.setMessage("Login Success !!");
		} else {
			userResponse.setMessage("User NOT REGISTERED !!");
		}
		return userResponse;
	}
	
}
