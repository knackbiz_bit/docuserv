package com.docuserve.service;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.docuserve.ApplicationProperties;
import com.docuserve.exceptionHandling.DocuserveException;
import com.docuserve.logger.DocuserveApiLogger;

@Service(value = "ocrIntegrationService")
public class OcrIntegrationService {

	@Autowired
	DocuserveApiLogger LOGGER;
	
	@Autowired
	ApplicationProperties properties;
	
	public String getResponse(String fileName, InputStream stream,Long groupId) throws Exception {
		
		try {
			LOGGER.writeInfo("OCR Integration service STARTS::");

			MultipartEntityBuilder builder = MultipartEntityBuilder.create()
					.addBinaryBody(fileName, stream,
							ContentType.create("application/octet-stream"), fileName);
			HttpEntity entity = builder.build();

			HttpPost httpPost = new HttpPost(properties.getRemoteUrl()+"/"+groupId);
			httpPost.setEntity(entity);
			InputStream[] artifacts = new InputStream[] {
					stream };
			for (InputStream artifact : artifacts) {
				builder.addPart("file", new InputStreamBody(artifact, "file"));
			}
			HttpClient httpClient = new DefaultHttpClient();

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity result = response.getEntity();
			//IOUtils.copy(result.getContent(), System.out);
			/*{"TEMPAT":"New Location","DESA":"N.A","NIK":"1234","ALAMAT":"#011, SINGAPORE","RT":" ","STATUS":"ACTIVE","NAMA":"Full Name",
			 * "KECAMATAN":null,"GOLDARAH":"Not existing","TGL":"United Front","AGAMA":null,"JENIS":"Dummy Jenis"}*/
			
			return new String(IOUtils.toByteArray(response.getEntity().getContent()));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while converting image to Text");
		} 
		
	}

	public InputStream extract(String type) throws Exception {		
		LOGGER.writeInfo("OCR Integration service STARTS::");
		HttpGet httpPost = new HttpGet(properties.getRemoteUrl());
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = httpClient.execute(httpPost);
		return response.getEntity().getContent();		
	}
	
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   return builder.build();
	}

}
