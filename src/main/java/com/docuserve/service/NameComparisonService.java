package com.docuserve.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;

@Component
public class NameComparisonService {

	public static void main(String[] args) {
		new NameComparisonService().isMatching("BUDHaI RAHARDJO", "BUDHI RAHARDJO");
	}

	public boolean isMatching(String input, String fullName) {
		if(StringUtils.isBlank(input) || StringUtils.isBlank(fullName)){
			return false;
		}
		boolean matching = false;
		List<String> allNames = Arrays.asList(fullName.toUpperCase().split(" "));
		List<String> firstChars = Lists.newArrayList();
		for (String allName : allNames) {
			if(StringUtils.isBlank(allName))
				continue;
			firstChars.add("" + allName.toUpperCase().charAt(0));
		}
		String[] tokenizedInput = input.toUpperCase().split(" ");
		List<String> namesInput = Lists.newArrayList(tokenizedInput).stream().filter(x-> x.length() > 3).collect(Collectors.toList());
		int successCount = namesInput.size() / 2;
		int count = 0;
		boolean matched = false;
		for (String token : namesInput) {
			if (count >= successCount && successCount != 0) {
				matched = true;
				break;
			}
			if (allNames.contains(token) || firstChars.contains(token)) {
				if(successCount == 0){
					matched = true;
					break;
				}
				
				count++;
			}
		}
		System.out.println(
				"Matching Count :: " + count + " :: successCount = " + successCount + " matched :: " + matched);
		// System.out.println(new Generator(allNames).permutate());
		// new Generator(firstChars).permutate();
		return matched;
	}
}

class Generator {

	Object[] permutables = null;

	List<String> outputs = Lists.newLinkedList();

	public Generator(Object[] permutables) {
		this.permutables = permutables;
	}

	String[] toElements(String output) {
		String[] op = new String[output.length()];

		for (int i = 0; i < output.length(); i++) {
			op[i] = "" + output.charAt(i);
		}

		return op;
	}

	int fact(int n) {
		return (n == 1) ? 1 : n * fact(n - 1);
	}

	/* Swap elements */
	private void swap(String[] elements, int i, int j) {
		String temp = elements[i];
		elements[i] = elements[j];
		elements[j] = temp;
	}

	public List<List<Integer>> permutate() {
		List<List<Integer>> allPositions = new ArrayList<>();
		Assert.isTrue(permutables != null, "Arry of input values cannot be null");
		int max = fact(permutables.length);
		String op = "";
		List<Integer> positionList = new ArrayList<>();
		for (int i = 0; i < permutables.length; i++) {
			op += "" + i;
			positionList.add(i);
		}
		allPositions.add(positionList);
		outputs.add(op);
		op = "";
		int index = 0;
		for (int i = 0; i < permutables.length; i++) {
			op += permutables[i] + ", ";
		}
		System.out.println(op);

		for (int arrIndex = 0; arrIndex < outputs.size(); arrIndex++) {
			if (index >= max) {
				break;
			}

			String[] ele = toElements(outputs.get(arrIndex));
			for (int i = 0; i < ele.length; i++) {
				if (index >= max || outputs.size() == max) {
					break;
				}
				for (int j = i + 1; j < ele.length; j++) {
					swap(ele, i, j);
					String output = "";
					for (String e : ele) {
						output += e;
					}
					if (addToOutput(output)) {
						String[] op1 = toElements(output);
						String temp = "";
						List<Integer> currentPositionList = Lists.newLinkedList();
						for (String o : op1) {
							Integer position = Integer.parseInt(o);
							temp += permutables[position] + ", ";
							currentPositionList.add(position);
						}
						allPositions.add(currentPositionList);
						System.out.println(temp.substring(0, temp.length() - 2));
						index++;
					} else {
						if (outputs.size() == max) {
							break;
						}
					}
				}
			}
		}

		return allPositions;
	}

	private boolean addToOutput(String output) {
		return outputs.contains(output) ? false : outputs.add(output);
	}

}