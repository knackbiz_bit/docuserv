package com.docuserve.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docuserve.dao.KabupatenDAO;
import com.docuserve.dao.KecamatanDAO;
import com.docuserve.dao.KelDAO;
import com.docuserve.dao.ProvinsiDAO;
import com.docuserve.domain.master.Kabupaten;
import com.docuserve.domain.master.Kecamatan;
import com.docuserve.domain.master.Kel;
import com.docuserve.domain.master.Provinsi;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

@Service
public class GeoPoliticalCacheService {

	@Autowired
	ProvinsiDAO provinsiDAO;

	@Autowired
	KabupatenDAO kabupatenDAO;

	@Autowired
	KecamatanDAO kecamatanDAO;

	@Autowired
	KelDAO kelDAO;

	private final Cache<String, Provinsi> provinsiCache = CacheBuilder.newBuilder().initialCapacity(128)
			.maximumSize(100000).expireAfterWrite(1, TimeUnit.DAYS).recordStats().build();

	private final Cache<String, Kabupaten> kabupatenCache = CacheBuilder.newBuilder().initialCapacity(128)
			.maximumSize(100000).expireAfterWrite(1, TimeUnit.DAYS).recordStats().build();

	private final Cache<String, Kecamatan> kecamatanCache = CacheBuilder.newBuilder().initialCapacity(128)
			.maximumSize(100000).expireAfterWrite(1, TimeUnit.DAYS).recordStats().build();

	private final Cache<String, Kel> kelCache = CacheBuilder.newBuilder().initialCapacity(128).maximumSize(100000)
			.expireAfterWrite(1, TimeUnit.DAYS).recordStats().build();
	
	@PostConstruct
	public void constructGeoPoliticalHierarchy(){
		loadProvinsiCache();
		loadKabupatenCache();
		loadKecamatanCache();
		loadKelCache();
		//prepare the hierarchy
		for (Provinsi provinsi : provinsiCache.asMap().values()) {
			for( Kabupaten kabupaten : kabupatenCache.asMap().values()){
				if(kabupaten.getProvinsiID().longValue()==provinsi.getProvinsiID().longValue()){
					provinsi.getKabupatens().add(kabupaten);
					for( Kecamatan kecamatan : kecamatanCache.asMap().values()){
						if(kecamatan.getKabupatanID().longValue()==kabupaten.getKabupatenID().longValue()){
							 kabupaten.getKecamatans().add(kecamatan);
							 for( Kel kel : kelCache.asMap().values()){
								if(kel.getKecamatanID().longValue()==kecamatan.getKecamatanID().longValue()){
									kecamatan.getKels().add(kel);
								}
							}
						}
					}
						
				}
			}
		}
	}
	public boolean loadProvinsiCache() {
		Iterable<Provinsi> provincesItr = provinsiDAO.findAll();
		for (Iterator<Provinsi> itr = provincesItr.iterator(); itr.hasNext();) {
			Provinsi provinsi = itr.next();
			provinsiCache.put(provinsi.getProvinceName(), provinsi);
		}
		return true;
	}
	//@PostConstruct
	public boolean loadKabupatenCache() {
		Iterable<Kabupaten> kabupatenItr = kabupatenDAO.findAll();

		for (Iterator<Kabupaten> itr = kabupatenItr.iterator(); itr.hasNext();) {
			Kabupaten kabupaten = itr.next();//for Kabupaten just have type also with _
			kabupatenCache.put(kabupaten.getKabupatenName()+"_"+kabupaten.getKabupatenID(), kabupaten);
		}
		return true;
	}
	//@PostConstruct
	public boolean loadKecamatanCache() {
		Iterable<Kecamatan> kecamatanItr = kecamatanDAO.findAll();
		
		for (Iterator<Kecamatan> itr = kecamatanItr.iterator(); itr.hasNext();) {
			Kecamatan kecamatan = itr.next();
			kecamatanCache.put(kecamatan.getKecamatanName()+"_"+kecamatan.getKabupatanID(), kecamatan);
		}
		return true;
	}
	//@PostConstruct
	public boolean loadKelCache() {
		Iterable<Kel> kelItr = kelDAO.findAll();

		for (Iterator<Kel> itr = kelItr.iterator(); itr.hasNext();) {
			Kel kel = itr.next();
			kelCache.put(kel.getKelName()+"_"+kel.getKecamatanID(), kel);
		}
		return true;
	}

	public List<Provinsi> getAllProvinsies() {
		Map<String, Provinsi> provincesMap = provinsiCache.asMap();
		return new ArrayList<>(Collections.unmodifiableCollection(provincesMap.values()));
	}
	public Provinsi getProvinceFromCache(String province) {
		Map<String, Provinsi> provinsiMap = provinsiCache.asMap();
		return provinsiMap.get(province);
	}
	public Kabupaten getKabupatanFromCache(String kabupatan) {
		Map<String, Kabupaten> KabupatenMap = kabupatenCache.asMap();
		return KabupatenMap.get(kabupatan);
	}
	public Kecamatan getKecamatanFromCache(String kecamatan) {
		Map<String, Kecamatan> kecamatanMap = kecamatanCache.asMap();
		return kecamatanMap.get(kecamatan);
	}
}
