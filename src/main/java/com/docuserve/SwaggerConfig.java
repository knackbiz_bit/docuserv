package com.docuserve;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.docuserve"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Docuserve Rest API")
                .description("Docuserve Rest API")
                .version("1.0")
                .contact(new Contact("Appcleus Team", "http://www.appcleus.com/", "team@appcleus.in"))
                .build();
    }
//    //Enable this only in Dev and Test not in Production
//    public static boolean isSwaggerEndpoint(HttpServletRequest httpRequest) {
//        return httpRequest.getRequestURI().contains("/swagger-ui.html")
//                || httpRequest.getRequestURI().contains("/v2/api-docs")
//                || httpRequest.getRequestURI().contains("/swagger-resources")
//                || httpRequest.getRequestURI().contains("/configuration/ui")
//                || httpRequest.getRequestURI().contains("/configuration/security")
//                || httpRequest.getRequestURI().matches("/kyck-rest/webjars/(.*)");
//    }
}
