package com.docuserve.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.docuserve.dto.UserDTO;
import com.docuserve.logger.DocuserveApiLogger;
import com.docuserve.response.UserResponse;
import com.docuserve.service.UserService;



@RestController
public class UserController extends WebMvcConfigurerAdapter {
	
	@Autowired
	UserService userService;
	
	@Autowired
	DocuserveApiLogger logger;

	@RequestMapping(value = "/users", method = RequestMethod.POST )
    public @ResponseBody ResponseEntity<UserResponse> login(@RequestBody UserDTO user) throws Exception{
		
		logger.writeInfo("User Login STARTS::" );
		
		return ResponseEntity.ok(userService.login(user));
	
    }
	
	@RequestMapping(value = "/users", method = RequestMethod.GET )
    public String loginPage() throws Exception{
		
		logger.writeInfo("User Login STARTS::" );
		
		return "index";
	
    }
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/users").setViewName("welcome");
	}
	

}