package com.docuserve.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.io.FilenameUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.docuserve.ApplicationProperties;
import com.docuserve.domain.master.Provinsi;
import com.docuserve.dto.DocumentDTO;
import com.docuserve.dto.DocumentResponseDTO;
import com.docuserve.logger.DocuserveApiLogger;
import com.docuserve.response.DocumentUrl;
import com.docuserve.security.model.AuthenticatedUser;
import com.docuserve.service.DocumentResponse;
import com.docuserve.service.DocumentService;
import com.docuserve.service.GeoPoliticalCacheService;
import com.docuserve.utility.ExcelUtility;
import com.docuserve.validator.DocumentValidator;
import com.google.common.collect.Lists;


@RestController
public class DocumentController extends WebMvcConfigurerAdapter {
	
	@Autowired
	DocumentService documentService;
	
	@Autowired
	DocumentValidator documentValidator;
	
	@Autowired
	ExcelUtility excelUtility;
	
	@Autowired
	DocuserveApiLogger LOGGER;
	
	@Autowired
	GeoPoliticalCacheService geoPoliticalCacheService;
	
	@Autowired
	ApplicationProperties properties;
	
	@RequestMapping(value = "/document/upload", method = RequestMethod.POST )
    public ResponseEntity<DocumentResponse> upload(@RequestPart @Valid @NotNull @NotBlank MultipartFile file, @RequestParam String documentParam, HttpServletRequest request) throws Exception{
		
		AuthenticatedUser authenticatedUser = (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		LOGGER.writeInfo("Recieved request for file::" +file.getOriginalFilename() +" Params are:" + documentParam);
		
		DocumentResponse response = documentService.save(documentParam, file, authenticatedUser.getUserId(),authenticatedUser.getGroupId());
		
		updateDocumentUrls(request, response);
		
		return new ResponseEntity<DocumentResponse>(response,  HttpStatus.OK);
    }

	@RequestMapping(value = "/document/uploadUrl", method = RequestMethod.POST )
    public ResponseEntity<DocumentResponse> upload(@RequestPart @NotNull @NotBlank String fileUrl, @RequestPart String documentParam, HttpServletRequest request) throws Exception{
		
		LOGGER.writeInfo("Recieved request for file upload(via URL)::"+ " Params are:" + documentParam);
		AuthenticatedUser authenticatedUser = (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		MultipartFile multipartFile = downloadFileFromUri(fileUrl); 
		DocumentResponse response = documentService.save(documentParam, multipartFile, authenticatedUser.getUserId(),authenticatedUser.getGroupId());
		updateDocumentUrls(request, response);
		
		return new ResponseEntity<DocumentResponse>(response,  HttpStatus.OK);
    }

	@RequestMapping(value="/document/dates", method= RequestMethod.GET)
    public void exportToExcel(@RequestParam("startDate") String startDateParam,  @RequestParam("endDate") String endDateParam, 
    		HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		AuthenticatedUser authenticatedUser = (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    List<DocumentDTO> documentDTOs = documentService.getDocuments(dateFormat.parse(startDateParam), dateFormat.parse(endDateParam),authenticatedUser.getGroupId());
	    
	    updateUrls(request, documentDTOs);
	    
        new ExcelUtility().renderMergedOutputModel(documentDTOs, request, response);
    }

	@RequestMapping(value="/document", method= RequestMethod.GET)
    public void exportToExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AuthenticatedUser authenticatedUser = (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    List<DocumentDTO> documentDTOs = documentService.getDocuments(authenticatedUser.getUserId());
	    updateUrls(request, documentDTOs);
        new ExcelUtility().renderMergedOutputModel(documentDTOs, request, response);
    }
	
	@RequestMapping(value = "/document/province", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody 
	public ResponseEntity<List<Provinsi>> fetchProvince() throws Exception {
		
		geoPoliticalCacheService.loadProvinsiCache();
		List<Provinsi> provinces = geoPoliticalCacheService.getAllProvinsies();
		return new ResponseEntity<List<Provinsi>>(provinces, HttpStatus.OK);
	}

	@RequestMapping(value = "/document/download", method = RequestMethod.GET)
    public void download(@RequestParam String fileUrl, @RequestParam(required=false) String type, HttpServletResponse response) throws Exception{
		
		LOGGER.writeInfo("Downloading file :: " + fileUrl);
		try{
		
			IOUtils.copy(documentService.downloadDocument(fileUrl, type, response), response.getOutputStream());
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
    }
	
	private void updateUrls(HttpServletRequest request,	List<DocumentDTO> documentDTOs) {
		
		for(DocumentDTO dto : documentDTOs){
	    	String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
	    	dto.setFileUrl(uri + "/docuserve/document/download?fileUrl="+dto.getReqId());
	    	dto.setFaceUrl(uri + "/docuserve/document/download?type=FACE&fileUrl="+dto.getReqId());
	    	dto.setSignatureUrl(uri + "/docuserve/document/download?type=SIG&fileUrl="+dto.getReqId());
	    }
		
	}
	
	private MultipartFile downloadFileFromUri(String fileUrl) throws MalformedURLException, IOException {
		URL url = new URL(fileUrl);
		URLConnection conn = url.openConnection();
		InputStream input = conn.getInputStream();
		String fileName = FilenameUtils.getName(url.getPath());
		
		MultipartFile multipartFile = new MockMultipartFile(fileName,
				fileName, conn.getContentType(), org.apache.commons.io.IOUtils.toByteArray(input));
		return multipartFile;
	}
	
	private void updateDocumentUrls(HttpServletRequest request, DocumentResponse response) {
		for(DocumentUrl url : response.getDocumentUrls()){
			String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
			if(url.getDocumentType().contains("Face"))
				url.setUrl(uri + "/docuserve/document/download?type=FACE&fileUrl="+response.getContentId());
			else if(url.getDocumentType().contains("Sign"))
				url.setUrl(uri + "/docuserve/document/download?type=SIG&fileUrl="+response.getContentId());
		}
	}
	
	private void updateDocumentUrls(HttpServletRequest request, DocumentResponseDTO responseDto) {
		for(DocumentUrl url : responseDto.getDocumentUrls()){
			String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
			if(url.getDocumentType().contains("Face"))
				url.setUrl(uri + "/docuserve/document/download?type=FACE&fileUrl="+responseDto.getContentId());
			else if(url.getDocumentType().contains("Sign"))
				url.setUrl(uri + "/docuserve/document/download?type=SIG&fileUrl="+responseDto.getContentId());
			else if(url.getDocumentType().contains("KTP"))
				url.setUrl(uri + "/docuserve/document/download?type=KTP&fileUrl="+responseDto.getContentId());
			else if(url.getDocumentType().contains("Clean"))
				url.setUrl(uri + "/docuserve/document/download?type=CLEAN&fileUrl="+responseDto.getContentId());
			else if(url.getDocumentType().contains("Tess"))
				url.setUrl(uri + "/docuserve/document/download?type=TESS&fileUrl="+responseDto.getContentId());
		}
	}
	
	@RequestMapping(value = "/document/fetchDocument", method = RequestMethod.POST )
    public ResponseEntity<DocumentResponseDTO> fetchDocument(@RequestParam String reqId, HttpServletRequest request) throws Exception{
		// authenticatedUser = (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		LOGGER.writeInfo("Recieved request to fetchDocument for reqId::" + reqId);
		DocumentResponseDTO responseDto = documentService.fetchDocument(reqId);
		updateDocumentUrls(request, responseDto);
		return new ResponseEntity<DocumentResponseDTO>(responseDto,  HttpStatus.OK);
    }
	@RequestMapping(value = "/document/fetchDocuments", method = RequestMethod.POST )
    public ResponseEntity<List<DocumentResponseDTO>> fetchDocuments(HttpServletRequest request, @RequestParam Integer pageNumber) throws Exception{
		AuthenticatedUser authenticatedUser = (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		LOGGER.writeInfo("Recieved request to fetch recent Documents ");
		List<DocumentResponseDTO> responsesDtoList = documentService.fetchDocuments(authenticatedUser.getGroupId(),pageNumber);
		for(DocumentResponseDTO responseDto : responsesDtoList){
		  updateDocumentUrls(request, responseDto);
		}
		return new ResponseEntity<List<DocumentResponseDTO>>(responsesDtoList,  HttpStatus.OK);
    }
	
	@RequestMapping(value = "/document/updateDocument", method = RequestMethod.POST )
    public ResponseEntity<DocumentResponseDTO> updateDocument(@RequestBody DocumentResponseDTO documentResponseDto,HttpServletRequest request) throws Exception{
		LOGGER.writeInfo("Recieved request to update the Document with reqId :"+documentResponseDto.getContentId());
		
		DocumentResponseDTO responseDto = documentService.updateDocument(documentResponseDto);
		updateDocumentUrls(request, responseDto);
		return new ResponseEntity<DocumentResponseDTO>(responseDto,  HttpStatus.OK);
    }
	
	@RequestMapping(value = "/document/reason", method = RequestMethod.POST )
    public ResponseEntity<List<String>> reasons() throws Exception{
		return new ResponseEntity<List<String>>(Lists.newArrayList(properties.getReasons().split(";")),  HttpStatus.OK);
    }
	
	@RequestMapping(value = "/document/uploadDoc", method = RequestMethod.POST )
    public ResponseEntity<DocumentResponseDTO> uploadDoc(@RequestPart @Valid @NotNull @NotBlank MultipartFile file, HttpServletRequest request) throws Exception{
		String documentParam="{\"a\":\"a\"}";
		AuthenticatedUser authenticatedUser = (AuthenticatedUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		LOGGER.writeInfo("Recieved request for file::" +file.getOriginalFilename() +" Params are:" + documentParam);
		DocumentResponse response = documentService.save(documentParam, file, authenticatedUser.getUserId(),authenticatedUser.getGroupId());
		DocumentResponseDTO responseDTO=documentService.fetchDocument(response.getContentId());
		updateDocumentUrls(request, responseDTO);
		return new ResponseEntity<DocumentResponseDTO>(responseDTO,  HttpStatus.OK);
    }
}

