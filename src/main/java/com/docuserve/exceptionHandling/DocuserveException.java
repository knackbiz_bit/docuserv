package com.docuserve.exceptionHandling;

public class DocuserveException extends Exception {
	
	private Integer code = 500;
	
	public DocuserveException(){
		super();
	}
	
	public DocuserveException(String message){
		super(message);
	}
	
	public DocuserveException(Integer code){
		this.code = code;
	}
	
	public DocuserveException(Throwable cause){
		super(cause);
	}
	
	public DocuserveException(String message, Throwable cause){
		super(message,cause);
	}
	
	public DocuserveException(Integer code, Throwable cause){
		super(cause);
		this.code = code;
	}
	
	public DocuserveException(Integer code, String message){
		super(message);
		this.code = code;
	}
	
	public DocuserveException(Integer code, String message, Throwable cause){
		super(message,cause);
		this.code = code;
	}


	public Integer getCode(){
		return this.code;
	}

}
