package com.docuserve.exceptionHandling;

public class DatabaseException extends DocuserveException {
	
	public DatabaseException() {
        super();
    }
	
	public DatabaseException(String message) {
        super(message);
    }

}
