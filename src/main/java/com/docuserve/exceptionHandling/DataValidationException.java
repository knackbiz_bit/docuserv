package com.docuserve.exceptionHandling;

public class DataValidationException extends Exception {

private Integer code = 500;
	
	public DataValidationException(){
		super();
	}
	
	public DataValidationException(String message){
		super(message);
	}
	
	public DataValidationException(Integer code){
		this.code = code;
	}
	
	public DataValidationException(Throwable cause){
		super(cause);
	}
	
	public DataValidationException(String message, Throwable cause){
		super(message,cause);
	}
	
	public DataValidationException(Integer code, Throwable cause){
		super(cause);
		this.code = code;
	}
	
	public DataValidationException(Integer code, String message){
		super(message);
		this.code = code;
	}
	
	public DataValidationException(Integer code, String message, Throwable cause){
		super(message,cause);
		this.code = code;
	}


	public Integer getCode(){
		return this.code;
	}

}
