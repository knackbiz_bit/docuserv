package com.docuserve.exceptionHandling;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class DocuserveExceptionHandler {
	
  public static final String DEFAULT_ERROR_VIEW = "error";

  @ExceptionHandler(DataValidationException.class)
  public void handleValidation(HttpServletResponse response) throws Exception{
	  
	  response.sendError(HttpStatus.BAD_REQUEST.value(), "");
  }
  
  @ExceptionHandler(DatabaseException.class)
  public String handleDatabaseException() throws DocuserveException {
	  
	 return "Database error";
	  
  }
  
  @ExceptionHandler(Exception.class)
  public void handleException(HttpServletResponse response) throws Exception{
	  
	  response.getWriter().write("Your request couldn't be completed. Please re-upload the image!");
  }
  
  @ExceptionHandler(DocuserveException.class)
  public void handleDocuserveException(HttpServletResponse response) throws Exception{
	  
	  response.getWriter().write("Your request couldn't be completed. Please re-upload the image!");
  }
  
}
