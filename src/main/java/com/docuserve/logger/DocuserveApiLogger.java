package com.docuserve.logger;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.docuserve.DocuserveApiApplication;

@Component
public class DocuserveApiLogger {
    
	static Logger file = Logger.getLogger(DocuserveApiApplication.class);
	
	static org.apache.commons.logging.impl.Log4JLogger LOGGER = new org.apache.commons.logging.impl.Log4JLogger(file);
	
	public void writeInfo(String str) {
		
		LOGGER.info("\n===========================================================================");
		LOGGER.info(str);
		LOGGER.info("\n===========================================================================");
    }
	
	public void writeError(Exception exception) {
        StringWriter writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        exception.printStackTrace(printWriter);
        printWriter.flush();
        writeInfo(writer.toString());
        
    }
	
}
