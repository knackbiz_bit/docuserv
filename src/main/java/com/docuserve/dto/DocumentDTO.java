package com.docuserve.dto;
import java.util.Date;


public class DocumentDTO {
	
	private String reqId;
	
	private String fileType;
	
	private String fileName;
	
	private String fileContent;
	
	private String fileDataJson;
	
	private String fileUrl;
	
	private String submittedBy;
	
	private Date submittedOn;
	
	private String ocrResponse;
	
	private String ocrResponseImproved;
	
	private String faceUrl;
	
	private String signatureUrl;
	
	private String docType;

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileDataJson() {
		return fileDataJson;
	}

	public void setFileDataJson(String fileDataJson) {
		this.fileDataJson = fileDataJson;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
	}

	public Date getSubmittedOn() {
		return submittedOn;
	}

	public void setSubmittedOn(Date submittedOn) {
		this.submittedOn = submittedOn;
	}

	public String getOcrResponse() {
		return ocrResponse;
	}

	public void setOcrResponse(String ocrResponse) {
		this.ocrResponse = ocrResponse;
	}

	public String getOcrResponseImproved() {
		return ocrResponseImproved;
	}

	public void setOcrResponseImproved(String ocrResponseImproved) {
		this.ocrResponseImproved = ocrResponseImproved;
	}

	public String getFaceUrl() {
		return faceUrl;
	}

	public void setFaceUrl(String faceUrl) {
		this.faceUrl = faceUrl;
	}

	public String getSignatureUrl() {
		return signatureUrl;
	}

	public void setSignatureUrl(String signatureUrl) {
		this.signatureUrl = signatureUrl;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}
	
	

}
