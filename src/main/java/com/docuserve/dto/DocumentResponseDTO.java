package com.docuserve.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.docuserve.response.DocumentUrl;

public class DocumentResponseDTO {

	private Boolean success;
	
	private String contentId;
	
	private String documentType;
	
	private List<DocumentUrl> documentUrls = new ArrayList<DocumentUrl>();
	
	private Map<String, String> fields;
	
	private Map<String, String> reasons;
	
	private Map<String, String> responseCorrect;
	
	private Map<String, Boolean> dataMatchStatus;
	
	private String submittedOn;
	
	private Boolean tamperStatus = false;
	
	private Boolean requestAnalysed;

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public List<DocumentUrl> getDocumentUrls() {
		return documentUrls;
	}

	public void setDocumentUrls(List<DocumentUrl> documentUrls) {
		this.documentUrls = documentUrls;
	}

	public Map<String, String> getFields() {
		return fields;
	}

	public void setFields(Map<String, String> fields) {
		this.fields = fields;
	}

	public Map<String, Boolean> getDataMatchStatus() {
		return dataMatchStatus;
	}

	public void setDataMatchStatus(Map<String, Boolean> dataMatchStatus) {
		this.dataMatchStatus = dataMatchStatus;
	}

	public void setSuccess(Boolean b) {
		this.success = b;
	}	

	public Boolean getSuccess() {
		return success;
	}

	public Boolean getTamperStatus() {
		return tamperStatus;
	}

	public void setTamperStatus(Boolean tamperStatus) {
		this.tamperStatus = tamperStatus;
	}

	public String getSubmittedOn() {
		return submittedOn;
	}

	public void setSubmittedOn(String submittedOn) {
		this.submittedOn = submittedOn;
	}

	public Map<String, String> getReasons() {
		return reasons;
	}

	public void setReasons(Map<String, String> reasons) {
		this.reasons = reasons;
	}

	public Map<String, String> getResponseCorrect() {
		return responseCorrect;
	}

	public void setResponseCorrect(Map<String, String> responseCorrect) {
		this.responseCorrect = responseCorrect;
	}
	
	public boolean isRequestAnalysed() {
		return requestAnalysed;
	}

	public void setRequestAnalysed(boolean requestAnalysed) {
		this.requestAnalysed = requestAnalysed;
	}

}
