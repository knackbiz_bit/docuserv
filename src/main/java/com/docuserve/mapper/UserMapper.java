package com.docuserve.mapper;

import java.util.UUID;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.docuserve.domain.Document;
import com.docuserve.domain.User;
import com.docuserve.dto.UserDTO;

@Component
public class UserMapper {
	
	public void mapDomain(User userDomain, UserDTO userDto){
		
		userDomain.setEmailId(userDto.getEmailId());
		userDomain.setPassword(userDto.getPassword());
		
	}

}
