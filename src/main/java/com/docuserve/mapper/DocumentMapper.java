package com.docuserve.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.docuserve.domain.Document;
import com.docuserve.dto.DocumentDTO;

@Component
public class DocumentMapper {
	
	public void mapDomain(Document documentDomain, String documentJson, MultipartFile file, String userId){
		
		documentDomain.setReqId(UUID.randomUUID().toString());
		documentDomain.setFileType(file.getContentType());
		documentDomain.setFileName(file.getOriginalFilename());
		//documentDomain.setFileContent("fileContent".getBytes());
		documentDomain.setFileUrl("");
		documentDomain.setFileDataJson(documentJson);
		documentDomain.setSubmittedBy(userId);
		documentDomain.setSubmittedOn(new Date());
		
	}

	
	public List<DocumentDTO> mapDomainToDTO(List<Document> documents){
		
		List<DocumentDTO> documentDTOs = new ArrayList<DocumentDTO>();
		DocumentDTO documentDTO;
		
		for(Document document : documents){
			documentDTO = new DocumentDTO();
			documentDTO.setReqId(document.getReqId());
			documentDTO.setFileName(document.getFileName());
			documentDTO.setFileUrl(""+document.getReqId());
			documentDTO.setSubmittedBy(document.getSubmittedBy());
			documentDTO.setSubmittedOn(document.getSubmittedOn());
			documentDTO.setOcrResponse(document.getOcrResponse());
			documentDTO.setOcrResponseImproved(document.getOcrResponseImproved());
			documentDTO.setFaceUrl(document.getFaceUrl());
			documentDTO.setSignatureUrl(document.getSignatureUrl());
			documentDTO.setDocType(document.getDocType());
			documentDTOs.add(documentDTO);
		}
		
		return documentDTOs;
	}

}
