package com.docuserve.mapper;

import java.beans.PropertyDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.docuserve.domain.OcrOutput;
import com.docuserve.domain.master.Kabupaten;
import com.docuserve.domain.master.Kecamatan;
import com.docuserve.domain.master.Kel;
import com.docuserve.domain.master.Provinsi;
import com.docuserve.exceptionHandling.DocuserveException;
import com.docuserve.literals.IfcDocuserveLiterals;
import com.docuserve.response.OCRResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

@Component
public class OCRResponseMapper {
	
	@Value("${ocr.validation.fieldmap}")
	String ocrValidationFieldMap;
	
	@Value("${ocr.validation.numericmatrix}")
	String numericMatrix;

	@Value("${ocr.validation.jenis}")
	String jenisValues;

	@Value("${ocr.validation.goldarah}")
	String golDarah;

	@Value("${ocr.validation.agama}")
	String agama;

	@Value("${ocr.validation.status}")
	String status;
	
	@Value("${ocr.validation.pekarjaan}")
	String pekarjaan;
	
	public void prepareOcrOutput(OcrOutput ocrOutput, List<Provinsi> provices) throws Exception {
		BeanWrapper prepareResponseBean = new BeanWrapperImpl(ocrOutput);
		for (PropertyDescriptor property: prepareResponseBean.getPropertyDescriptors()) {
			if (!(prepareResponseBean.getPropertyValue(property.getName())==OcrOutput.class)){
				String value = (String)prepareResponseBean.getPropertyValue(property.getName());
				String valueArray[] = null;
				if(StringUtils.isNotBlank(value)){
					valueArray = value.split("=",2);
				}
				if(valueArray != null /*&& valueArray.length==1*/){
					value = valueArray[valueArray.length-1];
					if (value.equalsIgnoreCase(property.getName())) {
						value = "";
					}
				}
				if (value!=null) prepareResponseBean.setPropertyValue(property.getName(), value.trim());
			}		
		}
		
		refineAndAutoCorrect(ocrOutput, provices);
		ocrOutput.toUpperCase();
		ocrOutput.removeSpecialCharacters();
		
	}

	public void mapStringToOcrResponseObject(OCRResponse ocrResponse, String str) {
		
		
		try {
			Map<String, String> fieldMap = new HashMap<String, String>();
			ObjectMapper mapper = new ObjectMapper();
			fieldMap = mapper.readValue(getOcrValidationFieldMap(), new TypeReference<Map<String, String>>(){});
			
			Map<String, String> hashedValues = new HashMap<String, String>();
			System.out.println("---------------------------");
			System.out.println(System.lineSeparator());
			List<String> ocrData = Arrays.asList(str.split("\\n"));
			// Extract Province
			ocrResponse.setProvinsi(ocrData.get(0).split(" ", 2)[1]);
			// Extract Kabupaten
			ocrResponse.setKabupaten(ocrData.get(1).split(" ", 2)[1]);
			for (String temp : ocrData) {
				String strTemp[] = temp.split(":");
				if (strTemp.length>=2)
					hashedValues.put(strTemp[0].trim().toLowerCase(), strTemp[1].trim());
			}
			BeanWrapper prepareResponseBean = new BeanWrapperImpl(ocrResponse);
			for (Map.Entry m : hashedValues.entrySet()) {
				System.out.println(((String)m.getKey()).toLowerCase() + "-" + m.getValue());
				prepareResponseBean.setPropertyValue(fieldMap.get(((String)m.getKey()).toLowerCase()), m.getValue());
				
			}
			//Auto correction starts here
			//refineAndAutoCorrect(ocrResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void refineAndAutoCorrect(OcrOutput ocr, List<Provinsi> provinces) throws Exception{
		
		try{
			List<String> strList = provinces.stream()
	                .map( Object::toString )
	                .collect( Collectors.toList() );
			List<Kabupaten> kabupatens= getAllKabupaten(provinces);
			
			if(StringUtils.isNotBlank(ocr.getPROVINCE())){
            	ocr.setPROVINCE(ocr.getPROVINCE().replaceAll("]", ""));
            }
            String ocrProvince=ocr.getPROVINCE();
            boolean provinceMatched=false;
			ocr.setPROVINCE(getAlphabeticCorrections(ocr.getPROVINCE(),strList,false));
			if(StringUtils.isNotBlank(ocrProvince) && ocrProvince.equalsIgnoreCase(ocr.getPROVINCE())){
				provinceMatched=true;
			}
			//ocr.setKabupaten(getAlphabeticCorrections(ocr.getKabupaten(),Arrays.asList(new String[]{"ACEH BARAT DAYA","ACEH BESAR","ACEH JAYA","ACEH SELATAN","ACEH SINGKIL","ACEH TAMIANG"})));
			ocr.setNIK(getNumericCorrections(ocr.getNIK()));
			if(StringUtils.isNotBlank(ocr.getNIK()))
				ocr.setNIK(ocr.getNIK().replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR_FOR_NUMBER,""));
			
			
			
			ocr.setNAMA(getAlphabeticCorrections(ocr.getNAMA(), Lists.newArrayList(),false));
			ocr.setNAMA(ocr.getNAMA().replaceAll("\\(", "I").replaceAll("1", "I"));
			if(ocr.getNAMA().endsWith(",")){
				ocr.setNAMA(ocr.getNAMA().substring(0,ocr.getNAMA().lastIndexOf(",")));
			}
			ocr.setALAMAT(getAlphabeticCorrections(ocr.getALAMAT(), Lists.newArrayList(),false));
			//ocr.setLAHIR(getAlphabeticCorrections(ocr.getLAHIR(), Lists.newArrayList()));
			
			//ocr.setDOB(getNumericCorrections(ocr.getDOB())); 
			//ocr.setJENIS(getAlphabeticCorrections(ocr.getJENIS(), Arrays.asList(getJenisValues().split(","))));
			
			
			//apply RTRW Rule 
			if(StringUtils.isNotBlank(ocr.getRTRW())){//as for DL we wont get RTRW, if we get then apply this rule
			  ocr.setRTRW(insertForwardSlashRTRW(getNumericCorrections(ocr.getRTRW())));
			}
			if(StringUtils.isNotBlank(ocr.getAGAMA())){
			ocr.setAGAMA(getAlphabeticCorrections(ocr.getAGAMA().replaceAll(" ",  "").replaceAll("-", "").replaceAll("'", ""), Arrays.asList(getAgama().split(",")),false));
			}
			//ocr.setSTATUS(getAlphabeticCorrections(ocr.getSTATUS(), Arrays.asList(getStatus().split(","))));
			ocr.setPEKERJAAN(getAlphabeticCorrections(ocr.getPEKERJAAN(), Arrays.asList(getPekarjaan().split(",")),false));
			
			ocr.setKEWAR("WNI");
			
			
			String areaType = findAreaType(ocr);
		    if(StringUtils.isNotBlank(ocr.getAREA()) ){
		    	ocr.setAREA(ocr.getAREA().replaceAll("'", "").replaceAll("[0-9]", ""));
				List<String> kabupatenList= getKabupetans(ocr.getPROVINCE(),provinces,areaType);
				if(kabupatenList.isEmpty()){//Get All Kabupaten List
					kabupatenList=getKabupatensBasedOnAreatype(kabupatens,areaType);
				}
				String area=getAlphabeticCorrections(ocr.getAREA(), kabupatenList,true);
				if(area.contains("_")){
					Long kabupatenID=Long.parseLong(area.substring(area.indexOf("_")+1));
					area=area.substring(0,area.indexOf("_"));
					if(StringUtils.isBlank(ocr.getPROVINCE()) || !provinceMatched){//SET province based on area
						String provinceName= getProvinceBasedOnKabupatenID(kabupatenID, kabupatens, provinces);
						if(StringUtils.isNotBlank(provinceName))
						ocr.setPROVINCE(provinceName);
					}
				}
				ocr.setAREA(area);
			}
			if (provinceMatched && ocr.getAREA() != null && !ocr.getAREA().equalsIgnoreCase("") && StringUtils.isNotBlank(ocr.getNIK())) {
				//correct the NIK if province and kabupaten are found in DB and not matched with NIK
				Provinsi provinsi=getProvinsi(ocr.getPROVINCE(), provinces);
				if(provinsi!=null){
					Kabupaten kabupaten=getKabupaten(ocr.getAREA(), provinsi.getKabupatens(),areaType);
					String []nIKArray=ocr.getNIK().split(IfcDocuserveLiterals.REGEX_SPLIT_STRING_INTO_TWO_CHARACTER);
					if(nIKArray.length>1 && kabupaten!=null && kabupaten.getKabupatenID().longValue()!=Long.parseLong(nIKArray[0]+nIKArray[1])){
						ocr.setNIK(kabupaten.getKabupatenID().toString()+ocr.getNIK().substring(4,ocr.getNIK().length()));
					}else if(provinsi!=null && provinsi.getProvinsiID().longValue()!=Long.parseLong(nIKArray[0])){
						ocr.setNIK(ocr.getNIK().replaceFirst(nIKArray[0], provinsi.getProvinsiID().toString()));
					}
				}
			} 
			
			if(StringUtils.isNotBlank(ocr.getNIK()) && ocr.getNIK().length()==16)//SET the PROVINCE and KABUPATAN based on NIK if those are empty
			setProviceAndKabupatenBasedOnNIK(ocr, provinces, areaType);
			
			if(StringUtils.isNotBlank(ocr.getkECAMATAN())){
				ocr.setkECAMATAN(ocr.getkECAMATAN().replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "").replaceAll("'", "").replaceAll("[0-9]", ""));
			}
			if(StringUtils.isNotBlank(ocr.getKELDESA())){
				ocr.setKELDESA(ocr.getKELDESA().replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, "").replaceAll("'", "").replaceAll("[0-9]", ""));
			}
			
			List<String> kecamatans=getkECAMATAN(ocr.getPROVINCE(), ocr.getAREA(), provinces,areaType);
			if(kecamatans.isEmpty()){
				kecamatans=getAllKecamatan(provinces);
			}
			boolean kecamatanMatched=false;
			if(StringUtils.isNotEmpty(ocr.getkECAMATAN())){
				ocr.setkECAMATAN(ocr.getkECAMATAN().trim());
				if(ocr.getkECAMATAN().replaceAll(" ", "").length()>4){
				  kecamatanMatched=true;
				  ocr.setkECAMATAN(getAlphabeticCorrections(ocr.getkECAMATAN(),kecamatans,true));
				}
				else{
				  ocr.setkECAMATAN(getAlphabeticCorrections(ocr.getkECAMATAN(),Lists.newArrayList(),true));
				}
					
			}
			if(StringUtils.isNotEmpty(ocr.getKELDESA())){
				ocr.setKELDESA(ocr.getKELDESA().trim().replaceAll(" ", ""));
				if(ocr.getKELDESA().length()>3){
					List<String> kelDesa=getKel(ocr.getPROVINCE(), ocr.getAREA(), ocr.getkECAMATAN(), provinces,areaType);
					if(kelDesa.isEmpty()){
						kelDesa=getAllKelDesa(provinces);
					}
					if(kecamatanMatched){
					    ocr.setKELDESA(getAlphabeticCorrections(ocr.getKELDESA(),kelDesa,false));
					}else{
						kelDesa=getKel(ocr.getPROVINCE(), ocr.getAREA(), provinces,areaType);
						if(kelDesa==null || kelDesa.isEmpty()){
							kelDesa=getAllKelDesa(provinces);
						}
						ocr.setKELDESA(getAlphabeticCorrections(ocr.getKELDESA(),kelDesa,false));	
					}
				 }
				 else{
					ocr.setKELDESA(getAlphabeticCorrections(ocr.getKELDESA(),Lists.newArrayList(),false));
				 }
			}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			throw new DocuserveException("Exception while processing converted text from image");
		}
		
		
		extractBerlaku(ocr);
		
		
		try{
			if(ocr.getJENIS() != null){
				
				 Pattern golPattern =  Pattern.compile("[G][o][Ltl1I]");
					Matcher golMatcher = golPattern.matcher(ocr.getJENIS());
					if (golMatcher.find()) {
						ocr.setGOL(ocr.getJENIS().substring(golMatcher.start()));
						ocr.setGOL(replaceWithEmpty(ocr.getGOL(),Pattern.compile("[G][o][Ll1Ilt].*[D][a][r][a][h]")));
						ocr.setJENIS(ocr.getJENIS().substring(0,golMatcher.start()-1));
						ocr.setJENIS(getAlphabeticCorrections(ocr.getJENIS(), Arrays.asList(getJenisValues().split(",")),false));
					}
					
			}
			if(StringUtils.isNotBlank(ocr.getGOL())){
				ocr.setGOL(replaceWithEmpty(ocr.getGOL(),Pattern.compile("([Dd][Aa][Rr][Aa][Hh])|([Dd][Ee][Rr][Aa][Hh])")));
				ocr.setGOL(ocr.getGOL().replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR, ""));
				if ("0".equalsIgnoreCase(ocr.getGOL()) || "o".equalsIgnoreCase(ocr.getGOL()) || "C".equalsIgnoreCase(ocr.getGOL()))
					ocr.setGOL("O");
				else if ("A".equalsIgnoreCase(ocr.getGOL()) || "4".equalsIgnoreCase(ocr.getGOL()))
					ocr.setGOL("A");
				else if ("8".equalsIgnoreCase(ocr.getGOL()) || "B".equalsIgnoreCase(ocr.getGOL()))
					ocr.setGOL("B");
				else if ("AB".equalsIgnoreCase(ocr.getGOL()) || "A8".equalsIgnoreCase(ocr.getGOL()) )
					ocr.setGOL("AB");
				else
					ocr.setGOL("-");
			}else{
				ocr.setGOL("-");
			}
			//ocr.setGOL(getAlphabeticCorrections(ocr.getGOL(), Arrays.asList(getGolDarah().split(","))).trim());
			Pattern jenisPattern =  Pattern.compile("([Ll][Aa][Kk]|[A][K][I1l])|([Aa][Kk][IiT])");
			Matcher jenisMatcher = jenisPattern.matcher(ocr.getJENIS());
			if (jenisMatcher.find()) {
				ocr.setJENIS(IfcDocuserveLiterals.LAKI_LAKI);
			} 
			jenisPattern =  Pattern.compile("[P][cCE][R]|[E][M][P]|[pP][uoOU][A][N]");
			jenisMatcher = jenisPattern.matcher(ocr.getJENIS());
			if (jenisMatcher.find()) {
				ocr.setJENIS(IfcDocuserveLiterals.PEREMPUAN);
			}
			if(StringUtils.isNotBlank(ocr.getPEKERJAAN()))
				ocr.setPEKERJAAN(ocr.getPEKERJAAN().replaceAll("PEKERJAAN=", ""));
			
			if(StringUtils.isEmpty(ocr.getSTATUS())){
				ocr.setSTATUS(IfcDocuserveLiterals.KAWIN);
			}else{
				if(StringUtils.isNotBlank(ocr.getSTATUS()))
					ocr.setSTATUS(ocr.getSTATUS().replaceAll("STATUS=", ""));
				
				Pattern statusPatternBelum =  Pattern.compile("([B8][CcEF][L][U][M])|([L][U][M])|([B][Ee][Ll])");
				Pattern statusPatternCerai = Pattern.compile("([C][E][R][A][I]*)|([H][I1][D][U]*[P]*)");
				Matcher statusMatcher = statusPatternBelum.matcher(ocr.getSTATUS());
				
				if (statusMatcher.find()) {
					ocr.setSTATUS(IfcDocuserveLiterals.BELUM_KAWIN);
				} else {
					statusMatcher = statusPatternCerai.matcher(ocr.getSTATUS());
					if (statusMatcher.find()) {
						ocr.setSTATUS(IfcDocuserveLiterals.CERAI_HIDUP);
					} else {
						ocr.setSTATUS(IfcDocuserveLiterals.KAWIN);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new DocuserveException("Exception while processing converted text from image");
		}
			
		extractTempat(ocr);
		validateNIKLahirBerlaku(ocr);//
		//Send empty values as those are not getting send without empty string
		if (StringUtils.isBlank(ocr.getLAHIR())) {
			ocr.setLAHIR("");
		}
	}

	

	private void setProviceAndKabupatenBasedOnNIK(OcrOutput ocr,List<Provinsi> provinces, String areaType) {
			String []nIKArray=ocr.getNIK().split(IfcDocuserveLiterals.REGEX_SPLIT_STRING_INTO_TWO_CHARACTER);
			Provinsi provinsi=null;
			if(StringUtils.isBlank(ocr.getPROVINCE())){
			    provinsi=getProvinceBasedOnCode(Long.parseLong(nIKArray[0]), provinces);
				if(provinsi !=null)
				ocr.setPROVINCE(provinsi.getProvinceName());
			}else{
				provinsi=getProvinsi(ocr.getPROVINCE(), provinces);
			}
			if(provinsi!=null && provinsi.getKabupatens()!=null && StringUtils.isBlank(ocr.getAREA())){
				String areaFromNIK=getKabupatenName(Long.parseLong(nIKArray[0]+nIKArray[1]),provinsi.getKabupatens());
			    if(StringUtils.isNotBlank(areaFromNIK))
				ocr.setAREA(areaFromNIK);
			}
	}

	private void extractBerlaku(OcrOutput ocr) throws Exception {
		try {
			if (StringUtils.isEmpty(ocr.getBERLAKU())) {
				ocr.setBERLAKU(IfcDocuserveLiterals.SEUMUR_HIDUP);
			} else {
				Pattern p2 = Pattern
						.compile("([Ss5][Ee][Uu][Mm][Uu][Rr] [Hh][Ii1l][D0O][Uu][Pp])|([Ss5][Ee][Uu])|([Hh][Ii1l][D0O][Uu][Pp])|([Hh][Ii1l][D0O][Uu][Pp])|([Hh][Ii1l][D0O])|([Uu][Mm][Uu][Rr] [Hh][D0O][Uu][Pp])");
				Matcher m2 = p2.matcher(ocr.getBERLAKU());
				while (m2.find()) {
					ocr.setBERLAKU(IfcDocuserveLiterals.SEUMUR_HIDUP);
				}

				Pattern p1 = Pattern.compile("\\d{1,2}.*\\d{1,2}.*\\d{1,4}");
				Matcher m1 = p1.matcher(ocr.getBERLAKU().replaceAll("I", "1").replaceAll(" ", ""));
				while (m1.find()) {
					ocr.setBERLAKU(m1.group());
				}
				//If Berlaku is Empty, set it to "SEUMUR HIDUP"
				if (!IfcDocuserveLiterals.SEUMUR_HIDUP.equalsIgnoreCase(ocr
						.getBERLAKU())) {
					ocr.setBERLAKU(formatDateWithHypensDDMMYYYY(ocr
							.getBERLAKU()));
				} else if (StringUtils.isBlank(ocr.getBERLAKU())) {
					ocr.setBERLAKU(IfcDocuserveLiterals.SEUMUR_HIDUP);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while processing converted text from image BERLAKU");
		}
	}

	private void extractTempat(OcrOutput ocr) throws Exception{
		
		try {
			if (StringUtils.isNotBlank(ocr.getTEMPAT())) {
				//Pattern lahirPattern = Pattern.compile("\\d{2}.*\\d{2}.*\\d{4}");
				//Matcher lahirMatcher = lahirPattern.matcher(ocr.getTEMPAT());
				String arr[] = ocr.getTEMPAT().split("\\|");
				if(arr.length>0){
				ocr.setTEMPAT(arr[0].trim());
				ocr.setTEMPAT(ocr.getTEMPAT().replaceAll(":", "").trim());
				//ocr.setTEMPAT(replaceWithEmpty( ocr.getTEMPAT(), Pattern.compile("[Ll][aA]\\|[hH][iI][rR]\\|[lL][aA][nN][eE]")));
				ocr.setTEMPAT(replaceWithEmpty( ocr.getTEMPAT(), Pattern.compile("[hH][iI][rR]|[lL][aA][nN][eE]|[lL][aA][bB]")));
				}
				if(arr.length>1){
					ocr.setLAHIR(arr[1].trim());
					ocr.setLAHIR(ocr.getLAHIR().replaceAll(" ", "-"));
					Pattern lahirPattern = Pattern.compile("\\d{1,2}[-]\\d{1,2}[-]\\d{4}");
					Matcher m1 = lahirPattern.matcher(ocr.getLAHIR());
					if (m1.find()) {
						ocr.setLAHIR(ocr.getLAHIR().substring(m1.start(), m1.end()));
					} 
				}
				
/*				while (lahirMatcher.find()) {

					if (arr.length == 2) {
						ocr.setLAHIR(arr[1].trim());
					}
				}*/
				if (StringUtils.isNotBlank(ocr.getLAHIR())) {
					ocr.setLAHIR(formatDateWithHypensDDMMYYYY(ocr.getLAHIR()));
				}
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new DocuserveException("Exception while processing converted text from image TEMPAT");
			
		}
	}
	

	private String getAlphabeticCorrections(String str, List<String> probableMatches,boolean isKabupatenOrKecamaten) {
	
		if(str == null)
			return "";
		int minDistance = 4;
		String closestMatch = str;
		if(str.contains("=")){
			String[] arr = str.split("=");
			if(arr.length>1){
				closestMatch = arr[1];
			}
		}
		//closestMatch = closestMatch.replaceAll(":", "").trim();
		if(StringUtils.isNotBlank(closestMatch) && closestMatch.contains(":")){
			closestMatch = closestMatch.substring(closestMatch.indexOf(":") + 1).trim();
		}
		if (StringUtils.isEmpty(closestMatch)) {
			return closestMatch; 
		}
		if(closestMatch.startsWith("-") || closestMatch.endsWith("-")){
			closestMatch.replaceAll("-","");
		}
		Map<Integer,List<String>> map = new HashMap<Integer, List<String>>();
		for (String tempStr : probableMatches) {
			 int distance = longestSubstr(str.toLowerCase(), tempStr.toLowerCase());
			 //System.out.println("distance for " + tempStr + " is " + distance);
			 List<String> list=null;
			 if(map.containsKey(distance)){
				list=map.get(distance);
			 }else{
				list= new ArrayList<String>();
			 }
			 list.add(tempStr);
			 map.put(distance, list);
			 if (distance >= minDistance ) {
				 closestMatch = tempStr;
				 minDistance = distance;
			 } 
		}
		
		//If multiples with same then follow Levenshtein
		if((map.get(minDistance)!=null && map.get(minDistance).size()>1) || minDistance==4){
			int mindistance=8;
			if(isKabupatenOrKecamaten)
			mindistance=13;//for kabupaten/kota/kecamaten we are adding _1104 (four digit ids)
			
			List<String> probableMatchesNew=null;
			if(map.get(minDistance)!=null && map.get(minDistance).size()>1)
			probableMatchesNew=map.get(minDistance);
			else
			probableMatchesNew=probableMatches;	
			
			for(String tempStr : probableMatchesNew){
	             int distance = StringUtils.getLevenshteinDistance(str.toLowerCase(), tempStr.toLowerCase());
	             if (distance == 0) {
	                 closestMatch = tempStr;
	                 return closestMatch;
	             } else if (distance <= mindistance ) {
	                 closestMatch = tempStr;
	                 mindistance = distance;
	             }

			}
		}
		//System.out.println("Closest match is -- " + closestMatch);
		return closestMatch;
	}
	
	private String getNumericCorrections(String numericString) {
		if(numericString == null) return "";
		try {
			if(numericString.contains("=")){
				String[] arr = numericString.split("=");
				if(arr.length>1){
					numericString = arr[1];
				}
			}
			final ObjectMapper om = new ObjectMapper();
			final Map<String,String> numericMatrix = om.readValue(getNumericMatrix(), Map.class);
			for (Map.Entry<String, String> entry : numericMatrix.entrySet()) {
				numericString = numericString.replaceAll(entry.getValue(), entry.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return numericString;
	}

	public String getNumericMatrix() {
		return numericMatrix;
	}
	public String getOcrValidationFieldMap() {
		return ocrValidationFieldMap;
	}
	
	public static void main(String args[]) {
		
//		Pattern statusPatternBelum =  Pattern.compile("([B8][CcEF][L][U][M]*)|([L][U][M])|([B][Ee][Ll])");
//		Pattern statusPatternCerai = Pattern.compile("([C][E][R][A][I]*)|([H][I1][D][U]*[P]*)");
//		Matcher statusMatcher = statusPatternBelum.matcher(s);
		
//		if (statusMatcher.find()) {
//			System.out.println("BELUM");
//		}
//		statusMatcher = statusPatternCerai.matcher(s);
//		if (statusMatcher.find()) {
//			System.out.println("CERAI");
//		} else {
//			System.out.println("KAWIN");
//		}

		
		
//		if (statusMatcher.find()) {
//			System.out.println("BELUM KA");
//		} else {
//			statusMatcher = statusPatternCerai.matcher(s);
//		
//			if (statusMatcher.find()) {
//				System.out.println("CERAI HIDUP");
//			} else {
//				System.out.println("KAWIN");
//			}
//		}
		OcrOutput ocr= new OcrOutput();
		
		ocr.setNIK("33741008078490001");
		ocr.setBERLAKU("SEUMUR HIDUP");
		ocr.setLAHIR("08-07-1989");
		ocr.setJENIS("");
		
		try {
			validateNIKLahirBerlaku(ocr);
		} catch (DocuserveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String s = "1-08-1985";
		Pattern p1 = Pattern.compile("\\d{1,2}[-]\\d{1,2}[-]\\d{4}");
		Matcher m1 = p1.matcher(s);
		if (m1.find()) {
			System.out.println("matched..." + s.substring(m1.start(), m1.end()));
		} else {
			System.out.println("NO MATCH,....");
		}

		
		/*
		try {
			OCRResponseMapper val = new OCRResponseMapper();
//			OCRResponse ocr = new OCRResponse();
//			ocr.setProvinsi("Beng");
//			ocr.setNik("121266b612i2$22LlIdSsdd!ddd");
//			ocr.setKabupaten("ACEH 8ARAT");

			
//			Pattern jenisPattern =  Pattern.compile("[L][A][K]|[A][K][I1l]");
//			Matcher jenisMatcher = jenisPattern.matcher("PERM");
//			if (jenisMatcher.find()) {
//				System.out.println("LAKI LAKI");
//			} else {
//				System.out.println("PEREM");
//			}
			String text = "kABU kotA";
			Pattern kotaPattern = Pattern.compile("[Kk][Oo][TtIl][A]");
			Pattern kabuPatenPattern = Pattern.compile("[Kk][A][B8][U0O]");
			Matcher matcher = kotaPattern.matcher(text);
			boolean matches = matcher.lookingAt();
			if (!matches) {
				matcher = kabuPatenPattern.matcher(text);
				matches = matcher.lookingAt();
				if (!matches) {
					System.out.println("NO MATCH");
				} else {
					System.out.println("KABU match");
					System.out.println(matcher.replaceFirst("REPLACED"));
				}
			} else {
				System.out.println("KOTA match");
				System.out.println(matcher.replaceFirst("REPLACED"));
			}
			
			
			
//			while(m2.find()) {
//				System.out.println("KOTA FOUND");
//		    }
			//m2.
			
			System.out.println("Did it...");
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	}

	public String getJenisValues() {
		return jenisValues;
	}

	public String getGolDarah() {
		return golDarah;
	}

	public String getAgama() {
		return agama;
	}

	public String getStatus() {
		return status;
	}
	
	public String getTemporaryOcrDataFromFile() {
		try {
			return org.apache.commons.io.IOUtils.toString( new FileInputStream("E:\\renewed\\knack\\freelancing\\Appcleus\\2ndTask\\testing\\Archive\\9.tess"));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Empty";
	}

	public String getPekarjaan() {
		return pekarjaan;
	}
	
	String replaceWithEmpty(String str,Pattern pattern) {
		Matcher matcher = pattern.matcher(str);
		return matcher.replaceAll("");
	}
	private String insertForwardSlashRTRW(String str) {
		// RTRW 0xx/0yy
		try {
			StringBuffer strBuffer = new StringBuffer(str.replaceAll(" ", "").replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR_FOR_NUMBER,"").trim());
			Pattern rtRWPattern =  Pattern.compile(IfcDocuserveLiterals.REGEX_FOR_RTRW);
  		    Matcher rTRWMatcher = rtRWPattern.matcher(strBuffer.toString());
			if (rTRWMatcher.find()) {
				if(strBuffer.toString().length()==7 && strBuffer.toString().charAt(3)=='1' ){
					strBuffer.setCharAt(3,' ');
					strBuffer=new StringBuffer(strBuffer.toString().replace(" ", ""));
					rTRWMatcher=rtRWPattern.matcher(strBuffer.toString());
					rTRWMatcher.find();
				}
				return strBuffer.toString().substring(rTRWMatcher.start(),rTRWMatcher.start()+3)+"/"+strBuffer.toString().substring(rTRWMatcher.start()+3,rTRWMatcher.end());
			}
			else{			
				if (strBuffer.length() == 6) {
					strBuffer.insert(3, "/").toString();
				} else if (strBuffer.length() == 7) {
					strBuffer.replace(3, 3, "/").toString();
				} else if (strBuffer.length() == 5) {
					if (strBuffer.charAt(0) == '0') {
						strBuffer.insert(3, "/0").toString();
					} else {
						strBuffer.insert(0, "0").insert(3, "/").toString();
					}
				}
				if (strBuffer.length() > 0 && strBuffer.charAt(0) != '0')
					strBuffer.replace(0, 1, "0");
				if (strBuffer.length() > 4 && strBuffer.charAt(4) != '0')
					strBuffer.replace(4, 5, "0");
				return strBuffer.toString();
		 } 
		}catch (Exception e) {
			e.printStackTrace();
			return str;
		}
	}
	
	private List<String> getKabupetans(String provinsiName, List<Provinsi> provinsis, String kabupatenType){
		Provinsi provinsi=new Provinsi();
		provinsi.setProvinceName(provinsiName);
		List<Kabupaten> kabupatens=null;
		if(provinsis!=null && provinsis.contains(provinsi)){
			if (IfcDocuserveLiterals.KABUPATEN.equalsIgnoreCase(kabupatenType))
				kabupatens=provinsis.get(provinsis.indexOf(provinsi)).getKabupatensOnly();
			else 
				kabupatens=provinsis.get(provinsis.indexOf(provinsi)).getKotasOnly();
		}
		if(kabupatens!=null && !kabupatens.isEmpty()){
		  return kabupatens.stream().map( Object::toString ).collect(Collectors.toList());
		}
		return Lists.newArrayList();
	}
	
	private List<String> getkECAMATAN(String provinsiName,String kabupatenName, List<Provinsi> provinsis,String kabupatenType){
		Provinsi provinsi=new Provinsi();
		provinsi.setProvinceName(provinsiName);
		List<Kabupaten> kabupatens=null;
		List<Kecamatan> kecamatans=null;
		if(provinsis!=null && provinsis.contains(provinsi)){
			kabupatens=provinsis.get(provinsis.indexOf(provinsi)).getKabupatens();
			Kabupaten kabupaten= new Kabupaten();
			kabupaten.setKabupatenName(kabupatenName);
			kabupaten.setLevel2Flag(kabupatenType);
			if(kabupatens!=null && kabupatens.contains(kabupaten)){
				kecamatans=kabupatens.get(kabupatens.indexOf(kabupaten)).getKecamatans();
				if(kecamatans!=null && !kecamatans.isEmpty()){
					return kecamatans.stream().map( Object::toString ).collect( Collectors.toList() );
				}
			}
		}
		return Lists.newArrayList();
	}
	
	private List<String> getKel(String provinsiName,String kabupatenName, String kecamatanName,List<Provinsi> provinsis,String kabupatenType){
		Provinsi provinsi=new Provinsi();
		provinsi.setProvinceName(provinsiName);
		List<Kabupaten> kabupatens=null;
		List<Kecamatan> kecamatans=null;
		List<Kel> kels=null;
		if(provinsis!=null && provinsis.contains(provinsi)){
			kabupatens=provinsis.get(provinsis.indexOf(provinsi)).getKabupatens();
			Kabupaten kabupaten= new Kabupaten();
			kabupaten.setKabupatenName(kabupatenName);
			kabupaten.setLevel2Flag(kabupatenType);
			if(kabupatens!=null && kabupatens.contains(kabupaten)){
				kecamatans=kabupatens.get(kabupatens.indexOf(kabupaten)).getKecamatans();
				if(kecamatans!=null && !kecamatans.isEmpty()){
				  Kecamatan kecamatan= new Kecamatan();
				  kecamatan.setKecamatanName(kecamatanName);
				  if(kecamatans.contains(kecamatan)){
					  kels=kecamatans.get(kecamatans.indexOf(kecamatan)).getKels();
					  if(kels!=null && ! kels.isEmpty()){
					    return kels.stream().map( Object::toString ).collect(Collectors.toList());
					  }
				  }
				}
			}
		}
		return Lists.newArrayList();
	}
	
	private Provinsi getProvinsi(String provinsiName, List<Provinsi> provinsis){
		Provinsi provinsi=new Provinsi();
		provinsi.setProvinceName(provinsiName);
		if(provinsis!=null && provinsis.contains(provinsi)){
			return provinsis.get(provinsis.indexOf(provinsi));
		}
		return null;
	}
	
	private Kabupaten getKabupaten(String kabupatenName, List<Kabupaten> kabupatens,String kabupatenType){
		Kabupaten kabupaten=new Kabupaten();
		kabupaten.setKabupatenName(kabupatenName);
		kabupaten.setLevel2Flag(kabupatenType);
		if(kabupatens!=null && kabupatens.contains(kabupaten)){
			return kabupatens.get(kabupatens.indexOf(kabupaten));
		}
		return null;
	}
	
	
	private String formatDateWithHypensDDMMYYYY(String date){
		 date=date.replaceAll(IfcDocuserveLiterals.REGEX_SPECIAL_CHAR_FOR_NUMBER, "");
    	 String []lahirDateArray=date.split(IfcDocuserveLiterals.REGEX_SPLIT_STRING_INTO_TWO_CHARACTER);
    	 if (lahirDateArray.length < 4) return "";
    	 
    	 if(lahirDateArray[0].startsWith("9") || lahirDateArray[0].startsWith("8") || lahirDateArray[0].startsWith("B")){
    		 lahirDateArray[0]=lahirDateArray[0].replaceFirst("[98B]", "0");
    	 }
    	 
    	 int month=Integer.parseInt(lahirDateArray[1]);
    	 if(month>12 && lahirDateArray[3].length()==1){
    		 date=lahirDateArray[0]+"0"+lahirDateArray[1]+lahirDateArray[2]+lahirDateArray[3];
    	 }else{
    		 date=lahirDateArray[0]+lahirDateArray[1]+lahirDateArray[2]+lahirDateArray[3];
    	 }
    	 if(lahirDateArray[3].length()==1){
    		 date="0"+lahirDateArray[0]+lahirDateArray[1]+lahirDateArray[2]+lahirDateArray[3];
    	 }else{
    		 date=lahirDateArray[0]+lahirDateArray[1]+lahirDateArray[2]+lahirDateArray[3];
    	 }
    	 lahirDateArray=date.split(IfcDocuserveLiterals.REGEX_SPLIT_STRING_INTO_TWO_CHARACTER);
    	 date=lahirDateArray[0]+"-"+lahirDateArray[1]+"-"+lahirDateArray[2]+lahirDateArray[3];
    	 return date;
	}
	
	private String findAreaType(OcrOutput ocr) {
		
		if(ocr.getAREA() == null ) return IfcDocuserveLiterals.KOTA;
		
		Pattern kotaPatternOne = Pattern.compile("[Kk][Oo][TtIl][A]");
		Pattern kotaPatternTwo= Pattern.compile("[Kk][Oo][ ][TtIl][A]");
		Pattern kabuPatenPattern = Pattern.compile("[Kk][A][GB8]*[LBU0O][U0O][P][A][ ]*[T][E8U][N] |[BU0O][U0O][P][A][T][E8][N] | [P][A][T][E8][N]|[Kk][A][GB8]*[BU0O][U0O][P][A]");
		Matcher matcherKotaOne = kotaPatternOne.matcher(ocr.getAREA());
		Matcher matcherKotaTwo = kotaPatternTwo.matcher(ocr.getAREA());
		Matcher matcherKabupaten = kabuPatenPattern.matcher(ocr.getAREA());
		if (matcherKotaOne.find()) {
			ocr.setAREA(ocr.getAREA().substring(matcherKotaOne.end(),ocr.getAREA().length()));
			return IfcDocuserveLiterals.KOTA;
		}else if(matcherKotaTwo.find()){
			ocr.setAREA(ocr.getAREA().substring(matcherKotaTwo.end(),ocr.getAREA().length()));
			return IfcDocuserveLiterals.KOTA; //when no match found KOTA is sent
		} 
		
		else if(matcherKabupaten.find()){
			ocr.setAREA(ocr.getAREA().substring(matcherKabupaten.end(),ocr.getAREA().length()));
			return IfcDocuserveLiterals.KABUPATEN;
		}else {
			return IfcDocuserveLiterals.KOTA; //when no match found KOTA is sent
		}
	}
	
	
	public static int longestSubstr(String s, String t) {
		if (s.isEmpty() || t.isEmpty()) {
			return 0;
		}
		int m = s.length();
		int n = t.length();
		int cost = 0;
		int maxLen = 0;
		int[] p = new int[n];
		int[] d = new int[n];
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < n; ++j) {
				// calculate cost/score
				if (s.charAt(i) != t.charAt(j)) {
					cost = 0;
				} else {
					if ((i == 0) || (j == 0)) {
						cost = 1;
					} else {
						cost = p[j - 1] + 1;
					}
				}
				d[j] = cost;

				if (cost > maxLen) {
					maxLen = cost;
				}
			} // for {}

			int[] swap = p;
			p = d;
			d = swap;
		}
		return maxLen;
	}
	
	private static List<String> getKabupatensBasedOnAreatype(List<Kabupaten> kabupatens,  String areaType){
		if(kabupatens!=null && ! kabupatens.isEmpty()){
			List<Kabupaten> kabupatenList= new ArrayList<Kabupaten>();
			for(Kabupaten kabupaten : kabupatens){
				if(kabupaten!=null && kabupaten.getLevel2Flag().equalsIgnoreCase(areaType)){
					kabupatenList.add(kabupaten);
				}
			}
			return kabupatenList.stream().map( Object::toString ).collect(Collectors.toList());
		}
		return Lists.newArrayList();
	}
	
	private String getProvinceBasedOnKabupatenID(Long kabupatenCode, List<Kabupaten> kabupatens, List<Provinsi> provinsis){
		if(kabupatens!=null){
			for(Kabupaten kabupaten : kabupatens){
				if(kabupaten.getKabupatenID().longValue()==kabupatenCode.longValue()){
					;
					for(Provinsi provinsi : provinsis){
						if(kabupaten.getProvinsiID().longValue()==provinsi.getProvinsiID().longValue()){
							return provinsi.getProvinceName();
						}
					}
					
				}
			}
		}
		return null;
	}
	private Provinsi getProvinceBasedOnCode(Long provinceCode, List<Provinsi> provinsis){
		if(provinsis!=null){
		  for(Provinsi provinsi : provinsis){
			 if(provinsi.getProvinsiID().longValue()==provinsi.getProvinsiID().longValue())
			  return provinsi;
			}
		}
		return null;
	}
	private List<Kabupaten> getAllKabupaten(List<Provinsi> provinces){
		if(provinces!=null && ! provinces.isEmpty()){
			List<Kabupaten> kabupatens= new ArrayList<Kabupaten>();
			for(Provinsi province :provinces){
				if(province.getKabupatens()!=null && ! province.getKabupatens().isEmpty())
				kabupatens.addAll(province.getKabupatens());
			}
			return kabupatens;
		}
		return Lists.newArrayList();
	}
	
	private List<String> getAllKecamatan(List<Provinsi> provinces){
		if(provinces!=null && ! provinces.isEmpty()){
			List<Kecamatan> kecamatans= new ArrayList<Kecamatan>();
			for(Provinsi province :provinces){
				if(province.getKabupatens()!=null && ! province.getKabupatens().isEmpty()){
					for(Kabupaten kabupaten : province.getKabupatens()){
						if(kabupaten.getKecamatans()!=null && ! kabupaten.getKecamatans().isEmpty())
						kecamatans.addAll(kabupaten.getKecamatans());
					}
				}
				
			}
			return kecamatans.stream().map( Object::toString ).collect(Collectors.toList());
		}
		return Lists.newArrayList();
	}
	private List<String> getAllKelDesa(List<Provinsi> provinces){
		if(provinces!=null && ! provinces.isEmpty()){
			List<Kel> kels= new ArrayList<Kel>();
			for(Provinsi province :provinces){
				if(province.getKabupatens()!=null && ! province.getKabupatens().isEmpty()){
					for(Kabupaten kabupaten : province.getKabupatens()){
						if(kabupaten.getKecamatans()!=null && ! kabupaten.getKecamatans().isEmpty()){
							for(Kecamatan kecamatan : kabupaten.getKecamatans()){
								if(kecamatan.getKels()!=null && ! kecamatan.getKels().isEmpty())
									kels.addAll(kecamatan.getKels());
							}
						}
						
					}
				}
				
			}
			return kels.stream().map( Object::toString ).collect(Collectors.toList());
		}
		return Lists.newArrayList();
	}
	private String getKabupatenName(Long kabupatenID,  List<Kabupaten> kabupatens){
		if(kabupatens!=null && ! kabupatens.isEmpty()){
			for(Kabupaten kabupaten : kabupatens){
				if(kabupaten!=null && kabupaten.getKabupatenID().longValue()==kabupatenID.longValue()){
					return kabupaten.getKabupatenName();
				}
			}
		}
		return null;
	}
	
	private List<String> getKel(String provinsiName,String kabupatenName,List<Provinsi> provinsis,String kabupatenType){
		Provinsi provinsi=new Provinsi();
		provinsi.setProvinceName(provinsiName);
		List<Kabupaten> kabupatens=null;
		List<Kecamatan> kecamatans=null;
		List<Kel> kels=null;
		if(provinsis!=null && provinsis.contains(provinsi)){
			kabupatens=provinsis.get(provinsis.indexOf(provinsi)).getKabupatens();
			Kabupaten kabupaten= new Kabupaten();
			kabupaten.setKabupatenName(kabupatenName);
			kabupaten.setLevel2Flag(kabupatenType);
			if(kabupatens!=null && kabupatens.contains(kabupaten)){
				kecamatans=kabupatens.get(kabupatens.indexOf(kabupaten)).getKecamatans();
				if(kecamatans!=null && !kecamatans.isEmpty()){
					kels= new ArrayList<Kel>();
					for(Kecamatan kecamatan :kecamatans){
						if(kecamatan.getKels()!=null && ! kecamatan.getKels().isEmpty())
						kels.addAll(kecamatan.getKels());
					}
					return kels.stream().map( Object::toString ).collect(Collectors.toList());
				}
			}
		}
		return Lists.newArrayList();
	}
	
	private static void validateNIKLahirBerlaku(OcrOutput ocr) throws DocuserveException {
		try {
				String nIKDOB = "";
				String[] nIKDOBArray = null;
				if(StringUtils.isNotBlank(ocr.getNIK()) && ocr.getNIK().length()>=15){
					nIKDOB=ocr.getNIK().substring(6, 12);
					nIKDOBArray=nIKDOB.split(IfcDocuserveLiterals.REGEX_SPLIT_STRING_INTO_TWO_CHARACTER);
					int nikDay=Integer.parseInt(nIKDOBArray[0]);
					if(IfcDocuserveLiterals.PEREMPUAN.equalsIgnoreCase(ocr.getJENIS()) || nikDay>40){
						int day = Integer.parseInt(nIKDOBArray[0])-40;  //+40 for PEREMPUAN
						if(day<10){
							nIKDOB="0"+day+"-"+nIKDOBArray[1]+"-"+nIKDOBArray[2];
						}else{
							nIKDOB=day+"-"+nIKDOBArray[1]+"-"+nIKDOBArray[2];
						}
					}else{
						nIKDOB=nIKDOBArray[0]+"-"+nIKDOBArray[1]+"-"+nIKDOBArray[2];
					}
				}
				String lahirDOB="";
				if(ocr.getLAHIR()!=null && ocr.getLAHIR().length()==10){
					String [] lahirArray=ocr.getLAHIR().split("-");
					if(lahirArray!=null && lahirArray.length>1){
						int month = Integer.parseInt(lahirArray[1]);
						if(month<10){
							lahirDOB=lahirArray[0]+"-0"+month+"-"+lahirArray[2].substring(2, 4);
						}else{
							lahirDOB=lahirArray[0]+"-"+month+"-"+lahirArray[2].substring(2, 4);
						}
					}
				}
				String berlaku="";
				if(!ocr.getBERLAKU().equalsIgnoreCase(IfcDocuserveLiterals.SEUMUR_HIDUP) && ocr.getBERLAKU().length()==10){
					berlaku=ocr.getBERLAKU().substring(0,5);//ca'nt consider year
				}
				if(StringUtils.isNotEmpty(nIKDOB) && nIKDOB.equalsIgnoreCase(lahirDOB)){
					//change the Berlaku if not SEMUR HEDOOP and also can set JENIS if empty
					if(!ocr.getBERLAKU().equalsIgnoreCase(IfcDocuserveLiterals.SEUMUR_HIDUP)){
						berlaku=lahirDOB.substring(0,6)+ocr.getBERLAKU().substring(6,10);
						ocr.setBERLAKU(berlaku);
					}
					if(StringUtils.isEmpty(ocr.getJENIS())){
						if(Integer.parseInt(nIKDOBArray[0])>40){
							ocr.setJENIS(IfcDocuserveLiterals.PEREMPUAN);
						}else{
							ocr.setJENIS(IfcDocuserveLiterals.LAKI_LAKI);
						}
					}
				}else if(StringUtils.isNotEmpty(lahirDOB) && lahirDOB.substring(0,5).equalsIgnoreCase(berlaku) && ocr.getNIK().length()>12){
					//change the NIK
					int day=Integer.parseInt(lahirDOB.replaceAll("-", "").substring(0, 2));
					if(StringUtils.isEmpty(ocr.getJENIS())){
						if(Integer.parseInt(ocr.getNIK().substring(6, 8))-40==day){
							ocr.setJENIS(IfcDocuserveLiterals.PEREMPUAN);
						}else if(Integer.parseInt(ocr.getNIK().substring(6, 8))==day){
							ocr.setJENIS(IfcDocuserveLiterals.LAKI_LAKI);
						}
					}
					if(ocr.getJENIS().equalsIgnoreCase(IfcDocuserveLiterals.PEREMPUAN)){
					   day=day+40;
					   ocr.setNIK(ocr.getNIK().substring(0,6)+day+lahirDOB.replaceAll("-", "").substring(2,6)+ocr.getNIK().substring(12,ocr.getNIK().length()));
					}else if(ocr.getJENIS().equalsIgnoreCase(IfcDocuserveLiterals.LAKI_LAKI)){
					  ocr.setNIK(ocr.getNIK().substring(0,6)+lahirDOB.replaceAll("-", "").substring(0,6)+ocr.getNIK().substring(12,ocr.getNIK().length()));
					}
				}
				else if(StringUtils.isNotEmpty(nIKDOB)  && nIKDOB.substring(0,5).equalsIgnoreCase(berlaku)){
					//change the LAHIR
					int day = Integer.parseInt(nIKDOBArray[0]);  
					String nikDay=nIKDOBArray[0];
					if(Integer.parseInt(nIKDOBArray[0])>40){
						day=day-40;//+40 for PEREMPUAN
						if(StringUtils.isEmpty(ocr.getJENIS()))
						ocr.setJENIS(IfcDocuserveLiterals.PEREMPUAN);
					}else{
						if(StringUtils.isEmpty(ocr.getJENIS()))
						ocr.setJENIS(IfcDocuserveLiterals.LAKI_LAKI);
					}
					if(day<10){
						nikDay="0"+day;
					}else{
						nikDay=""+day;
					}
					if(StringUtils.isNotEmpty(ocr.getLAHIR()) && ocr.getLAHIR().length()>6){
					    ocr.setLAHIR(nikDay+"-"+nIKDOBArray[1]+"-"+ocr.getLAHIR().substring(6,ocr.getLAHIR().length()));
					}else if(Integer.parseInt(nIKDOBArray[2])>50){
						ocr.setLAHIR(nikDay+"-"+nIKDOBArray[1]+"-"+"19"+nIKDOBArray[2]);//need to check how we could identify better whether 19 or 20 century 
					}else{
						ocr.setLAHIR(nikDay+"-"+nIKDOBArray[1]+"-"+"20"+nIKDOBArray[2]);
					}
					
				}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DocuserveException("Exception while processing converted text from image (validateNIKLahirBerlaku)");
		}
	}
}
