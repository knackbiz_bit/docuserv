angular.module('docuserveApp').config(function($stateProvider, $urlRouterProvider) {
	
	// the ui router will redirect if a invalid state has come.
	$urlRouterProvider.otherwise('/page-not-found');
	// parent view - navigation state
	$stateProvider.state('header', {
		abstract : true,
		url : '',
		views : {
			'header@' : {
				templateUrl : '/docuserve/views/header.html',
				//controller : 'DocumentController'
			}
		}
	}).state('documentVerification', {
		parent : 'header',
		url : '/documentVerification',
		views : {
			'content@' : {
				templateUrl : '/docuserve/views/documentVerification.html',
				//controller : 'DocumentController'
			}
		}
	}).state('login', {
		parent : 'header',
		url : '/login',
		views : {
			'content@' : {
				templateUrl : '/docuserve/views/login.html',
				controller : 'LoginController',
			}
		}
	}).state('page-not-found', {
		parent : 'header',
		url : '/page-not-found',
		views : {
			'content@' : {
				templateUrl : '/docuserve/views/page-not-found.html',
				//controller : 'DocumentController'
			}
		}
	}).state('access-denied', {
		parent : 'header',
		url : '/access-denied',
		views : {
			'content@' : {
				templateUrl : '/docuserve/views/access-denied.html',
				//controller : 'DocumentController'
			}
		}
	});
});
