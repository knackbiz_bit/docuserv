'use strict';

App.controller('LoginController', ['$scope','$sce','blockUI', 'AuthService','LoginService','DocumentService','$http','$state','$rootScope', function($scope,$sce,blockUI,AuthService,LoginService,DocumentService,$http,$state,$rootScope) {
	// method for login
	$scope.login = function() {
		blockUI.start();
		var UserDTO = {
      			emailId : $scope.username,
      			password :$scope.password
      	};
		LoginService.login(UserDTO)
		.then(
			       function(res) {
						$scope.password = null;
						// checking if the token is available in the response
						if (res.token) {
							$scope.message = '';
							// setting the Authorization Bearer token with JWT token
							$http.defaults.headers.common['Authorization'] = 'Bearer ' + res.token;
							// setting the user in AuthService
							AuthService.user = res.id;
							//$rootScope.$broadcast('LoginSuccessful');
							// going to the documentVerification page
							$scope.showUploadDocument('uploadDoc');
							//load default data reasons/documents
							var promise = DocumentService.getReasons();
							promise.then(
							          function(payload) { 
							        	  $rootScope.reasons= payload;
							          },
							          function(errorPayload) {
							        	  console.error('failure loading reasons', errorPayload);
							});
							promise = DocumentService.fetchDocuments($rootScope.pageNumber);
							promise.then(
							          function(payload) { 
							        	  $rootScope.pageNumber=$rootScope.pageNumber+1
							        	  $rootScope.documents= payload;
							        	  blockUI.stop();
							          },
							          function(errorPayload) {
							        	  console.error('failure loading reasons', errorPayload);
							        	  blockUI.stop();
							});
							$state.go('documentVerification');
							
						} else {
							// if the token is not present in the response then the
							// authentication was not successful. Setting the error message.
							$scope.message = 'Authetication Failed !';
							blockUI.stop();
						}
					},
					function(errResponse){
						console.error('Error while login');
						blockUI.stop();
					}
		       );
		// requesting the token by usename and passoword
	};
}]);
