'use strict';

App.controller('DocumentController', ['$scope','$sce','$state', '$rootScope','blockUI','$timeout','DocumentService','AuthService', function($scope,$sce,$state,$rootScope,blockUI,$timeout,DocumentService,AuthService) {
	    $scope.documents='';
	    $scope.successMessage='';
	    $scope.document={};
	    $scope.docCount='';
	    $scope.reasons='';
	    $rootScope.reasons='';
	    $rootScope.documents='';
	    $scope.document.responseCorrect={};
	    $scope.displayUploadDoc=false;
	    $rootScope.pageNumber=0;
	    $rootScope.busy=false;
	    $scope.empty = function() {
	        return $scope.document==undefined || angular.equals({}, $scope.document.responseCorrect) ;
	    };
	    $scope.$watch('$root.reasons', function() {
	        $scope.reasons = $rootScope.reasons;
	    });
	    $scope.$watch('$root.documents', function() {
	        $scope.documents = $rootScope.documents;
	    });
	    $scope.fetchDocuments=function (){
	    	 if ($rootScope.busy) {
	    		 blockUI.stop();
	    		 return;
	    	 }
	    	 blockUI.start();
	    	 $rootScope.busy = true;
        	  DocumentService.fetchDocuments($rootScope.pageNumber)
                  .then(
      					       function(data) {
      					    	  if(data.length==0){
      					    		$rootScope.busy=true;
      					    	  }else{
      					    		for(var i in data){
      					    		 $rootScope.documents.push(data[i]);
      					    		}
	      					    	$rootScope.pageNumber=$rootScope.pageNumber+1;
	      					    	$rootScope.busy=false;
      					    	  }
      					    	 blockUI.stop();
      					       },
            					function(errResponse){
            						console.error('Error while fetching Documents.');
            					}
      			       );
          };
          
          $scope.fetchDocument =function (documentData,docCount){
        	   angular.element(document.querySelector('#ktpID')).attr('src','');
        	   blockUI.start();
        	   var myEl = angular.element( document.querySelector( '#spinnerKTP'));
	           myEl.removeClass('ktpSpinner-hide'); 
		  	   myEl.addClass('ktpSpinner-show');
        	   $scope.document = documentData;
        	   angular.element(document.querySelector('#ktpID')).attr('src',documentData.documentUrls[2].url);
      		   $scope.docCount = docCount; 
      		   $scope.successMessage='';
      		   $timeout(function() { 
      			  blockUI.stop(); 
      			}, 400);
      		 $timeout(function() { 
      			myEl.addClass('ktpSpinner-hide');
     			}, 9000);
          };///document/reason
          $scope.showUploadDocument=function (action){
          blockUI.start();
  		   if(action=='uploadDoc'){
  			   $scope.displayUploadDoc=true;
  			   var myEl = angular.element( document.querySelector( '#docVerification' ) );
	  	       myEl.addClass('whiteColour');
	  	       var myE2 = angular.element( document.querySelector( '#login' ) );
	  	       myE2.addClass('whiteColour');
	  	       
	  	       var myE3 = angular.element( document.querySelector( '#uploadDocLi' ) );
	  	       myE3.addClass('active');
	  	       var myE4 = angular.element( document.querySelector( '#uploadDoc') );
		       myE4.removeClass('whiteColour');
		       
		       var myE5 = angular.element( document.querySelector( '#docVerificationLi' ) );
		       myE5.removeClass('active');
		       var myE6 = angular.element( document.querySelector( '#loginLi' ) );
		       myE6.removeClass('active');
  		   }else if(action=='docVerification'){
  			   $scope.displayUploadDoc=false;
  			   $scope.displayUploadDoc=false;
			   if($scope.empty()){
				   $scope.document=$scope.documents[0];
			   }
			   var myKTP = angular.element( document.querySelector( '#spinnerKTP'));
			   myKTP.removeClass('ktpSpinner-hide'); 
			   myKTP.addClass('ktpSpinner-show');
        	   $timeout(function() { 
        		   myKTP.addClass('ktpSpinner-hide');
        	   }, 9000);
  			   var myEl = angular.element( document.querySelector( '#uploadDoc' ) );
	  	       myEl.addClass('whiteColour');
	  	       var myE2 = angular.element( document.querySelector( '#login' ) );
	  	       myE2.addClass('whiteColour');
	  	       
	  	       var myE3 = angular.element( document.querySelector( '#docVerificationLi' ) );
	  	       myE3.addClass('active');
	  	       var myE4 = angular.element( document.querySelector( '#docVerification') );
		       myE4.removeClass('whiteColour');
		       
		       var myE5 = angular.element( document.querySelector( '#uploadDocLi' ) );
		       myE5.removeClass('active');
		       var myE6 = angular.element( document.querySelector( '#loginLi' ) );
		       myE6.removeClass('active');
  		   }else{
			   var myEl = angular.element( document.querySelector( '#uploadDoc' ) );
	  	       myEl.addClass('whiteColour');
	  	       var myE2 = angular.element( document.querySelector( '#docVerification' ) );
	  	       myE2.addClass('whiteColour');
	  	       
	  	       var myE3 = angular.element( document.querySelector( '#loginLi' ) );
	  	       myE3.addClass('active');
	  	       var myE4 = angular.element( document.querySelector( '#login') );
		       myE4.removeClass('whiteColour');
		       
		       var myE5 = angular.element( document.querySelector( '#uploadDocLi' ) );
		       myE5.removeClass('active');
		       var myE6 = angular.element( document.querySelector( '#docVerificationLi' ) );
		       myE6.removeClass('active');
  		   }
  		   if (!AuthService.user){
  			 $state.go('access-denied');
  		   }
  		  $timeout(function() { 
  			  blockUI.stop(); 
  			}, 400);
          };
          function getReasons(){
        	  DocumentService.getReasons()
              .then(
  					       function(data) {
  					    	    $scope.reasons = data;
  					       },
        					function(errResponse){
        						console.error('Error while fetching Reasons.');
        					}
  			       );
        	  
         };
          $scope.toggleShowInput = function(the_string)
           { 
        	  if ($scope[the_string] === undefined) {
        	     $scope[the_string] = true;
        	  } else{
        	     $scope[the_string] = !$scope[the_string];
        	  }
           }
          $scope.formSubmit= function() {
        	  blockUI.start();
        	  var messageSpan = angular.element( document.querySelector( '#messageSpan' ) );
        	  $scope.successMessage='';
        	  DocumentService.updateDocument($scope.document)
              .then(
  					       function(data) {
  					    	    $scope.fetchDocument(data,$scope.docCount)
  					    	    $scope.documents[$scope.docCount-1]=data;
  					    	    $scope.successMessage="Response Details Updated Successfully!"
  					    	    messageSpan.removeClass('errorMessage');
  					    	    messageSpan.addClass('successMessage');
  					    	    blockUI.stop();
  					       },
        					function(errResponse){
  					    	    $scope.successMessage="Response not Updated due to an error!"
        						console.error('Error while updating Document');
  					    	    messageSpan.addClass('successMessage');
  					    	    blockUI.stop();
        					}
  			       );
          };
          $scope.uploadDocument= function() {
        	   blockUI.start();
        	   $scope.successMessage='';
        	   var file = $scope.docController.uploadFile;
        	   var messageSpan = angular.element( document.querySelector( '#messageSpan' ) );
               if(file==undefined){
            	   $scope.successMessage="Please provide KTP File !"
            	   messageSpan.addClass('errorMessage');
            	   messageSpan.removeClass('successMessage');
            	   blockUI.stop();
    			   return ;
    		   }
               DocumentService.uploadDocument(file)
              .then(
  					       function(data) {
  					    	    $scope.fetchDocument(data,0);
  					    	    $scope.documents.unshift(data);
  					    	    messageSpan.removeClass('errorMessage');
  					    	    messageSpan.addClass('successMessage');
  					    	    $scope.successMessage="KTP File Uploaded Successfully and below are the Response Details!";
  					    	    blockUI.stop();
  					       },
        					function(errResponse){
  					    	    $scope.successMessage="KTP File not Uploaded due to an error!";
  					    	    messageSpan.removeClass('successMessage');
  					    	    messageSpan.addClass('errorMessage');
        						console.error('Error while uploading document');
        						blockUI.stop();
        					}
  			       );
          };
          
          
      }]);
