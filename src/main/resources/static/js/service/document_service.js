'use strict';

App.factory('DocumentService', ['$http', '$q', function($http, $q){

	return {
		fetchDocuments: function(pageNumber) {
					return $http.post('/docuserve/document/fetchDocuments?pageNumber='+pageNumber)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while searching orders'+errResponse);
										return $q.reject(errResponse);
									}
							);
			},
		fetchDocument: function(contentId) {
				return $http.post('/docuserve/document/fetchDocument?reqId'+contentId)
						.then(
								function(response){
									return response.data;
								}, 
								function(errResponse){
									console.error('Error while fetching documents '+errResponse);
									return $q.reject(errResponse);
								}
						);
		},
		
	   updateDocument: function(document) {
			return $http.post('/docuserve/document/updateDocument',document)
					.then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while updating Documents '+errResponse);
								return $q.reject(errResponse);
							}
					);
	   },
	   
	   uploadDocument: function(file) {
		    var fd = new FormData();
	        fd.append('file', file);
	        fd.append('documentParam', '{"emailId": "user@kelolaapp.com","password" : "kelolaapp"}');
			return $http.post("/docuserve/document/uploadDoc", fd, {
	                  transformRequest: angular.identity,
	                  headers: {'Content-Type': undefined}
	               })
	               .then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while uploading Documents '+errResponse);
								return $q.reject(errResponse);
							}
					);
	            
	   },
	
	 getReasons : function(document) {
		 return $http.post('/docuserve/document/reason')
				    .then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while searching orders '+errResponse);
								return $q.reject(errResponse);
							}
					);
        },
        login : function(UserDTO) {
			return $http.post("/docuserve/users",UserDTO)
	               .then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while uploading Documents '+errResponse);
								return $q.reject(errResponse);
							}
					);
	            
	   },
  };

}]);
