'use strict';

App.factory('LoginService', ['$http', '$q', function($http, $q){

	return {
        login : function(UserDTO) {
			return $http.post("/docuserve/users",UserDTO)
	               .then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while login  '+errResponse);
								return $q.reject(errResponse);
							}
					);
	            
	   },
  };

}]);
