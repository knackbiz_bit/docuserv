'use strict';

var App = angular.module('docuserveApp', ['ngAnimate','blockUI', 'ngTouch','ngRoute','ui.router','infinite-scroll']);


App.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

App.directive('imageonload', function() {
    return {
        restrict: 'A',
      
        link: function(scope, element) {
          element.on('load', function() {
        	  element.parent().find('span').removeClass('ktpSpinner-show');
        	  element.parent().find('span').addClass('ktpSpinner-hide');
          });
          scope.$watch('ngSrc', function() {
            // Set visibility: false + inject temporary spinner overlay
        	  element.parent().find('span').addClass('ktpSpinner-show');
              // element.parent().append('<span class="spinner"></span>');
          });
        }
    };
});
App.run(function(AuthService, $rootScope, $state) {
	// For implementing the authentication with ui-router we need to listen the
	// state change. For every state change the ui-router module will broadcast
	// the '$stateChangeStart'.
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
		// checking the user is logged in or not
		if (!AuthService.user) {
			$rootScope.userLoggedIn=true;
			// To avoiding the infinite looping of state change we have to add a
			// if condition.
			if (toState.name != 'login' && toState.name != 'register') {
				event.preventDefault();
				$state.go('login');
			}
		} else {
			$rootScope.userLoggedIn=false;
			// checking the user is authorized to view the states
			if (toState.data && toState.data.role) {
				var hasAccess = false;
				for (var i = 0; i < AuthService.user.roles.length; i++) {
					var role = AuthService.user.roles[i];
					if (toState.data.role == role) {
						hasAccess = true;
						break;
					}
				}
				if (!hasAccess) {
					event.preventDefault();
					$state.go('access-denied');
				}

			}
		}
	});
});

angular.module('docuserveApp').config(function(blockUIConfig) {

	  // Change the default overlay message
	  // Change the default delay to 100ms before the blocking is visible
	  blockUIConfig.templateUrl = '/docuserve/views/blockUI.html';
	  //blockUIConfig.message = 'Please wait';
	  blockUIConfig.delay = 50;
	  blockUIConfig.autoBlock = false;
	

	});
     

